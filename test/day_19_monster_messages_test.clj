(ns day-19-monster-messages-test
  (:require 
   [clojure.test :refer [testing deftest is]]
   [day-19-monster-messages :as imp]))

(def example-input "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb")

(def example-rules-frag "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"")

(deftest ->rules-db-test
  (testing "parses input into something more structured"
    (let [expected-rules {0 [[4 1 5]]
                          1 [[2 3] [3 2]]
                          2 [[4 4] [5 5]]
                          3 [[4 5] [5 4]]
                          4 \a
                          5 \b}
          actual-rules (imp/->rules-db example-rules-frag)]
      
      (is (= expected-rules actual-rules)))))


(deftest match-test
  (testing "matches strings correctly"
    (let [rules-db (imp/->rules-db example-rules-frag)]
      (is (= true (imp/match rules-db "ababbb")))
      (is (= false (imp/match rules-db "bababa")))
      (is (= true (imp/match rules-db "abbbab")))
      (is (= false (imp/match rules-db "aaabbb")))
      (is (= false (imp/match rules-db "aaaabbb"))))))
