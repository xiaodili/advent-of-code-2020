(ns day-08-handheld-halting-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-08-handheld-halting :refer
    [->instructions
     run-with-loop-detect
     run-program]]))

(def example-instructions [["nop" 0]
                           ["acc" 1]
                           ["jmp" 4]
                           ["acc" 3]
                           ["jmp" -3]
                           ["acc" -99]
                           ["acc" 1]
                           ["jmp" -4]
                           ["acc" 6]])

(deftest ->instructions-test
  (testing "parses raw input into list of instructions"
    (let [raw-input "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"
          expected-instructions example-instructions]
      (is (= expected-instructions (->instructions raw-input))))))

(deftest run-with-loop-detect-test
  (testing "returns the value of the accumulator when a loop has been detected"
    (is (= 5 (run-with-loop-detect example-instructions)))))

(deftest run-program-test
  (testing "returns indices of instructions that constitute the loop" 
    (is (= [1 2 6 7 3 4] (run-program example-instructions))))

  (testing "returns indices of instructions that constitute the loop"
    (let [modified-instructions [["nop" 0]
                                 ["acc" 1]
                                 ["jmp" 4]
                                 ["acc" 3]
                                 ["jmp" -3]
                                 ["acc" -99]
                                 ["acc" 1]
                                 ["nop" -4]
                                 ["acc" 6]]]
      (is (= 8 (run-program modified-instructions))))))
