(ns day-18-operation-order-test
  (:require 
   [clojure.test :refer [testing deftest is]]
   [day-18-operation-order :as imp]))

(deftest evaluate-test
  (testing "evaluating works"

    (is (= 26 (imp/evaluate "2 * 3 + (4 * 5)")))
    (is (= 437 (imp/evaluate "5 + (8 * 3 + 9 + 3 * 4 * 3)")))
    (is (= 12240 (imp/evaluate "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")))
    (is (= 13632 (imp/evaluate "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))))

(deftest evaluate-advanced-test
  (testing "implicit precedence"

    (is (= 51 (imp/evaluate-advanced "1 + (2 * 3) + (4 * (5 + 6))")))
    (is (= 46 (imp/evaluate-advanced "2 * 3 + (4 * 5)")))
    (is (= 1445 (imp/evaluate-advanced "5 + (8 * 3 + 9 + 3 * 4 * 3)")))
    (is (= 669060 (imp/evaluate-advanced "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")))
    (is (= 23340 (imp/evaluate-advanced "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))))
