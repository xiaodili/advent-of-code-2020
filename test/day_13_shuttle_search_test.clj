(ns day-13-shuttle-search-test
  (:require
   [day-13-shuttle-search :as imp]
   [clojure.test :refer [testing is deftest]]))

(def example-notes [939 [7
                         13
                         'x
                         'x
                         59
                         'x
                         31
                         19]])

(deftest ->notes
  (testing "parsing notes"
    (let [raw-input "939
7,13,x,x,59,x,31,19"
          expected-notes example-notes]
      (is (= expected-notes (imp/->notes raw-input))))))

(deftest ->wait-time-test
  (testing "gets wait times"
    (is (= 6 (imp/->wait-time 939 7)))
    (is (= 10 (imp/->wait-time 939 13)))
    (is (= 5 (imp/->wait-time 939 59)))))

(deftest earliest-bus-test
  (testing "gets the bus ID and wait time"
    (let [[bus-id wait-time] (imp/earliest-bus 939 [7 13 59 31 19])]
      (is (= 59 bus-id))
      (is (= 5 wait-time)))))

(deftest perfect-departure-test
  (testing "calculates earliest timestamp when you have continuous stream of buses departing"

    (is (= 1068781 (imp/perfect-departure (second example-notes))))

    (is (= 754018 (imp/perfect-departure [67 7 59 61])))

    (is (= 779210 (imp/perfect-departure [67 'x 7 59 61])))

    (is (= 1202161486 (imp/perfect-departure [1789 37 47 1889])))))
