(ns day-10-adapter-array-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-10-adapter-array :refer
    [->chain
     ->differences
     num-arrangements]]))

(def example-adapters [16
                       10
                       15
                       5
                       1
                       11
                       7
                       19
                       6
                       12
                       4])

(deftest differences-test
  (testing "finds the sequence of numbers that overlap"
    (let [expected-sequence [0
                             1
                             4
                             5
                             6
                             7
                             10
                             11
                             12
                             15
                             16
                             19
                             22]
          expected-differences [1
                                3
                                1
                                1
                                1
                                3
                                1
                                1
                                3
                                1
                                3
                                3]
          actual-sequence (->chain example-adapters 3)
          actual-differences (->differences actual-sequence)]
      (is (= expected-sequence actual-sequence))
      (is (= expected-differences actual-differences)))))

(deftest num-chains-test
  (testing "number of distinct ways adapters can be chained"
    (is (= 8 (num-arrangements example-adapters 3)))))
