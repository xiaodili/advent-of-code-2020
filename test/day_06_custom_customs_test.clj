(ns day-06-custom-customs-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-06-custom-customs :refer
    [->group-answerss
     count-all-yes
     count-yes]]))

(deftest parse-input-test
  (testing "input parsing"
    (let [raw-input "abc

a
b
c

ab
ac

a
a
a
a

b"
          expected-group-answerss [["abc"]
                                   ["a" "b" "c"]
                                   ["ab" "ac"]
                                   ["a" "a" "a" "a"]
                                   ["b"]]]
      (is (= expected-group-answerss (->group-answerss raw-input))))))

(deftest count-yes-test
  (testing "count yes in group of answers"
    (is (= 3 (count-yes ["abc"])))
    (is (= 3 (count-yes ["a" "b" "c"])))
    (is (= 3 (count-yes ["ab" "ac"])))
    (is (= 1 (count-yes ["a" "a" "a" "a"])))))

(deftest count-all-yes-test
  (testing "count answers where everyone answered yes"
    (is (= 3 (count-all-yes ["abc"])))
    (is (= 0 (count-all-yes ["a" "b" "c"])))
    (is (= 1 (count-all-yes ["ab" "ac"])))
    (is (= 1 (count-all-yes ["a" "a" "a" "a"])))))
