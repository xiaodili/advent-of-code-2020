(ns day-14-docking-data-test
  (:require
   [day-14-docking-data :as imp]
   [clojure.test :refer [testing deftest is]]))

(def example-input "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0")

(deftest parse-input-test
  (testing "parses input correctly"
    (let [[mask & write-instructions] (imp/parse-input example-input)]

      (is (= "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X" mask))

      (is (= [[8 11]
              [7 101]
              [8 0]] write-instructions)))))

(deftest run-instruction-test
  (testing "creates 36 char string correctly"
    (let [mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
          value 11]
      (is (= "000000000000000000000000000001001001"
             (imp/run-instruction value mask))))))

(deftest ->number-test
  (testing "binary string → number"

      (is (= 73 (imp/->number "000000000000000000000000000001001001"))

      (is (= 20691600039 (imp/->number "010011010001010100001100001010100111"))))))

(deftest ->replace-str-at-pos-vals-test
  (testing "checking SO answer"
    (is (= "4734567" (imp/replace-str-at-pos-vals
                      "1234567"
                      [[0 4] [1 7]])))))

(deftest ->bin-values-test
  (testing "version 2 decoding"

    (let [expected-result ["000000000000000000000000000000011010"
                           "000000000000000000000000000000011011"
                           "000000000000000000000000000000111010"
                           "000000000000000000000000000000111011"]
          actual-result (imp/->bin-values
                         "000000000000000000000000000000X1001X" 42)]
      (is (= expected-result actual-result)))

    (let [expected-result ["000000000000000000000000000000010000"
                           "000000000000000000000000000000010001"
                           "000000000000000000000000000000010010"
                           "000000000000000000000000000000010011"
                           "000000000000000000000000000000011000"
                           "000000000000000000000000000000011001"
                           "000000000000000000000000000000011010"
                           "000000000000000000000000000000011011"]
          actual-result (imp/->bin-values
                         "00000000000000000000000000000000X0XX" 26)]
      (is (= expected-result actual-result)))))
