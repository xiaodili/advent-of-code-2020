(ns day-04-passport-processing-test
  (:require
   [day-04-passport-processing :refer
    [parse-input
     parse-passport-str
     valid-passport?
     valid-byr?
     valid-iyr?
     valid-hgt?
     valid-hcl?
     valid-ecl?
     valid-pid?]]
   [clojure.test :refer [testing deftest is]]))

(deftest parse-passport-str-test
  (testing "parses a line of passport data correctly"
    (let [passport-line "hcl:#cfa07d byr:1929"
          expected-passport-details {:hcl "#cfa07d"
                                     :byr "1929"}]
      (is (= expected-passport-details (parse-passport-str passport-line))))))

(deftest parse-input-test
  (testing "parses batches of passport data correctly"
    (let [raw-input "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"
          parsed-passports (parse-input raw-input)
          expected-passports [{:ecl "gry"
                               :pid "860033327"
                               :eyr "2020"
                               :hcl "#fffffd"
                               :byr "1937"
                               :iyr "2017"
                               :cid "147"
                               :hgt "183cm"}
                              {:iyr "2013"
                               :ecl "amb"
                               :cid "350"
                               :eyr "2023"
                               :pid "028048884"
                               :hcl "#cfa07d"
                               :byr "1929"}
                              {:hcl "#ae17e1"
                               :iyr "2013"
                               :eyr "2024"
                               :ecl "brn"
                               :pid "760753108"
                               :byr "1931"
                               :hgt "179cm"}
                              {:hcl "#cfa07d"
                               :eyr "2025"
                               :pid "166559648"
                               :iyr "2011"
                               :ecl "brn"
                               :hgt "59in"}]]
      (is (= expected-passports parsed-passports)))))



(deftest valid-passport?-test
  (testing "Correctly checks required passport fields"
    (let [valid-passport {:ecl "gry"
                          :pid "860033327"
                          :eyr "2020"
                          :hcl "#fffffd"
                          :byr "1937"
                          :iyr "2017"
                          :cid "147"
                          :hgt "183cm"}
          invalid-passport {:iyr "2013"
                            :ecl "amb"
                            :cid "350"
                            :eyr "2023"
                            :pid "028048884"
                            :hcl "#cfa07d"
                            :byr "1929"}]
      (is (= true (valid-passport? valid-passport)))
      (is (= false (valid-passport? invalid-passport))))))

(deftest valid-byr?-test
  (testing "Correctly validates birthyear field from string"
    (is (= true (valid-byr? "1990")))
    (is (= true (valid-byr? "2002")))
    (is (= false (valid-byr? "2003")))
    (is (= false (valid-byr? "rabbits")))
    (is (= false (valid-byr? "2002.2")))))

(deftest valid-iyr?-test
  (testing "Correctly validates issue year field from string"
    (is (= true (valid-iyr? "2010")))
    (is (= true (valid-iyr? "2020")))
    (is (= false (valid-iyr? "2030")))
    (is (= false (valid-iyr? "2000")))
    (is (= false (valid-iyr? "rabbits")))
    (is (= false (valid-iyr? "2002.2")))))

(deftest valid-hgt?-test
  (testing "Correctly validates height from string"
    (is (= true (valid-hgt? "60in")))
    (is (= true (valid-hgt? "190cm")))
    (is (= false (valid-hgt? "2030")))
    (is (= false (valid-hgt? "190in")))
    (is (= false (valid-hgt? "190")))
    (is (= false (valid-hgt? "rabbits")))))

(deftest valid-hcl?-test
  (testing "Correctly validates hair colour from string"
    (is (= true (valid-hcl? "#123abc")))
    (is (= true (valid-hcl? "#139abf")))
    (is (= true (valid-hcl? "#039abf")))
    (is (= false (valid-hcl? "#123abz")))
    (is (= false (valid-hcl? "123abc")))))

(deftest valid-ecl?-test
  (testing "Correctly validates eye colour from string"
    (is (= true (valid-ecl? "brn")))
    (is (= false (valid-ecl? "wat")))))

(deftest valid-pid?-test
  (testing "Correctly validates passport number from string"
    (is (= true (valid-pid? "000000001")))
    (is (= false (valid-pid? "0123456789")))
    (is (= false (valid-pid? "0123i5789")))))
