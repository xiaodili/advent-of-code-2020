(ns day-15-rambunctious-recitation-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-15-rambunctious-recitation :as imp]))

(def sample-starting-numbers [0 3 6])

(deftest next-number-test
  (testing "getting the next number for turns"

    (is (= 0 (last (imp/next-number sample-starting-numbers))))

    (is (= 3 (last
              (imp/next-number
               (imp/next-number sample-starting-numbers)))))

    (is (= 3 (last
              (imp/next-number
               (imp/next-number
                (imp/next-number sample-starting-numbers))))))

    (is (= 1 (last
              (imp/next-number
               (imp/next-number
                (imp/next-number
                 (imp/next-number sample-starting-numbers)))))))

    (is (= 1 (last
              (imp/next-number
               (imp/next-number
                (imp/next-number
                 (imp/next-number sample-starting-numbers)))))))))

(deftest number-at-turn-test
  (testing "recursive naive solution"
    (is (= 3 (imp/number-at-turn sample-starting-numbers 5)))
    (is (= 436 (imp/number-at-turn sample-starting-numbers 2020)))
    (is (= 1 (imp/number-at-turn [1 3 2] 2020)))
    (is (= 10 (imp/number-at-turn [2 1 3] 2020)))
    (is (= 27 (imp/number-at-turn [1 2 3] 2020)))
    (is (= 78 (imp/number-at-turn [2 3 1] 2020)))
    (is (= 438 (imp/number-at-turn [3 2 1] 2020)))
    (is (= 1836 (imp/number-at-turn [3 1 2] 2020)))))

(deftest number-at-turn-faster-test
  (testing "optimised solution"
    (is (= 3 (imp/number-at-turn-faster sample-starting-numbers 5)))
    (is (= 436 (imp/number-at-turn-faster sample-starting-numbers 2020)))
    (is (= 1 (imp/number-at-turn-faster [1 3 2] 2020)))
    (is (= 10 (imp/number-at-turn-faster [2 1 3] 2020)))
    (is (= 27 (imp/number-at-turn-faster [1 2 3] 2020)))
    (is (= 78 (imp/number-at-turn-faster [2 3 1] 2020)))
    (is (= 438 (imp/number-at-turn-faster [3 2 1] 2020)))
    (is (= 1836 (imp/number-at-turn-faster [3 1 2] 2020)))

    ))
