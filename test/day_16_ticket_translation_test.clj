(ns day-16-ticket-translation-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-16-ticket-translation :as imp]))

(def example-raw-notes "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12")

(deftest parse-notes-test
  (testing "parsing notes to fields, your ticket and nearby tickets"
    (let [expected-fields [["class" [1 3] [5 7]]
                           ["row" [6 11] [33 44]]
                           ["seat" [13 40] [45 50]]]
          expected-your-ticket [7 1 14]
          expected-nearby-tickets [[7 3 47]
                                   [40 4 50]
                                   [55 2 20]
                                   [38 6 12]]
          [actual-fields actual-your-ticket actual-nearby-tickets] (imp/parse-input example-raw-notes)]
      
      (is (= expected-fields actual-fields))
      (is (= expected-your-ticket actual-your-ticket))
      (is (= expected-nearby-tickets actual-nearby-tickets)))))

(deftest ->invalid-tickets-test
  (testing "test filtering of invalid fields"
    (let [expected-errors [4 55 12]
          ranges [[1 3] [5 7] [6 11] [33 44] [13 40] [45 50]]
          nearby-tickets [7 3 47 40 4 50 55 2 20 38 6 12]
          actual-errors (imp/->invalid-tickets ranges nearby-tickets)]

      (is (= expected-errors actual-errors)))))

(deftest ->column-fields-idxs-test
  (testing "being able to map from column to fields"
    (is (= [[0 1] [1 0] [2 2]] (imp/->column-field-idxs
                                [[[0 1] [4 19]]
                                 [[0 5] [8 19]]
                                 [[0 13] [16 19]]]
                                [[11 12 13] [3 9 18] [15 1 5] [5 14 9]])))))
