(ns day-09-encoding-error-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-09-encoding-error :refer
    [first-invalid-xmas-number
     contiguous-sequence]]))

(def example-port-numbers [35
                           20
                           15
                           25
                           47
                           40
                           62
                           55
                           65
                           95
                           102
                           117
                           150
                           182
                           127
                           219
                           299
                           277
                           309
                           576
                           ])

(deftest first-invalid-xmas-number-test
  (testing "gets the first number that's not a valid xmas sequence"
    (is (= 127 (first-invalid-xmas-number example-port-numbers 5)))))

(deftest contiguous-sequence-test
  (testing "gets the sequence that sums to N"
    (is (= [15 25 47 40] (contiguous-sequence example-port-numbers 127)))))
