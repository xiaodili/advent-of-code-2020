(ns day-03-toboggan-trajectory-test
  (:require
   [day-03-toboggan-trajectory :refer [parse-input traverse]]
   [clojure.test :refer [testing deftest is]]))

(deftest parse-input-test
  (testing "returns an array from parse-map"
    (let [raw-input "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"
          parsed-map (parse-input raw-input)
          expected-map (to-array-2d [[\. \. \# \# \. \. \. \. \. \. \.]
                                     [\# \. \. \. \# \. \. \. \# \. \.]
                                     [\. \# \. \. \. \. \# \. \. \# \.]
                                     [\. \. \# \. \# \. \. \. \# \. \#]
                                     [\. \# \. \. \. \# \# \. \. \# \.]
                                     [\. \. \# \. \# \# \. \. \. \. \.]
                                     [\. \# \. \# \. \# \. \. \. \. \#]
                                     [\. \# \. \. \. \. \. \. \. \. \#]
                                     [\# \. \# \# \. \. \. \# \. \. \.]
                                     [\# \. \. \. \# \# \. \. \. \. \#]
                                     [\. \# \. \. \# \. \. \. \# \. \#]])]

      (doseq [i (range (count expected-map))
              j (range (count (first expected-map)))]
        (is (= (aget expected-map i j) (aget parsed-map i j)))))))

(deftest traverse-test
  (testing "returns the path from traversing the map"
    (let [test-map (to-array-2d [[\. \. \# ]
                                 [\# \# \. ]
                                 [\# \. \# ]
                                 [\# \. \. ]
                                 [\# \. \. ]
                                 [\. \# \. ]])
          expected-path [\. \# \# \# \. \.]]
      (is (= expected-path (traverse test-map [1 1]))))))
