(ns day-01-report-repair-test
  (:require
   [day-01-report-repair :refer [find-sum-to parse-input]]
   [clojure.test :refer [testing deftest is]]))

(deftest find-sum-to-test
  (testing "returns the correct pairs"
    (let [entries [1721 979 366 299 675 1456]
          target 2020
          pair (find-sum-to target entries 2)]
      (is (= [1721 299] pair)))))

(deftest parse-input-test
  (testing "returns a list of numbers from the puzzle input"
    (let [raw-input "1721
675
1456"
          numbers (parse-input raw-input)]
      (is (= [1721 675 1456] numbers)))))
