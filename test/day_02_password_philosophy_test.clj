(ns day-02-password-philosophy-test
  (:require
   [day-02-password-philosophy :refer [password-valid? password-official-valid? parse-policy-password]]
   [clojure.test :refer [testing deftest is]]))

;; (deftest parse-input-test
;;   (testing "parses password and policies correctly"
;;     (let [raw-input "4-7 z: zzzfzlzzz
;; 3-4 l: blllk
;; 8-11 j: jjjjjjjgjjjj"
;;           policy-passwords (parse-input raw-input)
;;           first-policy-password (first policy-passwords)
;;           [policy password] first-policy-password]
;;       (is (= {:character \z
;;               :at-least 4
;;               :at-most 7
;;               } policy))
;;       (is (= "zzzfzlzzz" password)))))

(deftest parse-policy-password-test
  (testing "parses password and policy correctly from string"
    (let [[policy password] (parse-policy-password "4-7 z: zzzfzlzzz")]
      (is (= {:character \z
              :at-least 4
              :at-most 7
              } policy))
      (is (= "zzzfzlzzz" password)))))

(deftest password-valid?-test
  (testing "policy validation"
    (is (= true (password-valid? {:character \c
                                  :at-least 1
                                  :at-most 3} "abcde")))
    (is (= false (password-valid? {:character \b
                                   :at-least 1
                                   :at-most 3} "cdefg")))
    (is (= true (password-valid? {:character \c
                                  :at-least 2
                                  :at-most 9} "ccccccccc")))))

(deftest password-official-valid?-test
  (testing "policy official validation"
    (is (= true (password-official-valid? {:character \c
                                  :at-least 1
                                  :at-most 3} "abcde")))
    (is (= false (password-official-valid? {:character \b
                                   :at-least 1
                                   :at-most 3} "cdefg")))
    (is (= false (password-official-valid? {:character \c
                                  :at-least 2
                                  :at-most 9} "ccccccccc")))))
