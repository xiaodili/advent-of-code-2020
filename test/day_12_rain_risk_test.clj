(ns day-12-rain-risk-test
  (:require
   [clojure.test :refer [is testing deftest]]
   [day-12-rain-risk :refer
    [->actions
     take-evasive-actions
     manhattan-distance
     take-evasive-waypoint-actions]]))

(def example-actions [[\F 10]
                      [\N 3]
                      [\F 7]
                      [\R 90]
                      [\F 11]])

(deftest ->actions-test
  (testing "converting input into actions"
    (let [expected-actions example-actions
          raw-input "F10
N3
F7
R90
F11"]
      (is (= expected-actions (->actions raw-input))))))

(deftest manhattan-distance-test
  (testing "basic math"
    (is (= 25 (manhattan-distance 17 8)))
    (is (= 25 (manhattan-distance -17 8)))
    (is (= 25 (manhattan-distance 17 -8)))))

(deftest take-evasive-actions-test
  (testing "moves accordingly"
    (let [{x :x y :y} (take-evasive-actions [[\F 10]])]
      (is (= 10 x))
      (is (= 0 y)))

    (let [{x :x y :y} (take-evasive-actions [[\F 10]
                                             [\N 3]])]
      (is (= 10 x))
      (is (= -3 y)))

    (let [{x :x y :y} (take-evasive-actions [[\F 10]
                                             [\N 3]
                                             [\F 7]])]
      (is (= 17 x))
      (is (= -3 y)))

    (let [{x :x y :y dir :dir} (take-evasive-actions [[\F 10]
                                                      [\N 3]
                                                      [\F 7]
                                                      [\R 90]])]
      (is (= 17 x))
      (is (= -3 y))
      (is (= 270 dir)))

    (let [{x :x y :y} (take-evasive-actions example-actions)]
      (is (= 17 x))
      (is (= 8 y)))))

(deftest apply-waypoint-action-test
  (testing "applying waypoint action"

    (let [{x :x y :y} (take-evasive-waypoint-actions [[\F 10]])]
      (is (= 100 x))
      (is (= -10 y)))

    (let [{x :x y :y wx :wx wy :wy} (take-evasive-waypoint-actions [[\F 10]
                                                                    [\N 3]])]
      (is (= 100 x))
      (is (= -10 y))
      (is (= 10 wx))
      (is (= -4 wy)))

    (let [{x :x y :y wx :wx wy :wy} (take-evasive-waypoint-actions [[\F 10]
                                                                    [\N 3]
                                                                    [\F 7]])]
      (is (= 170 x))
      (is (= -38 y))
      (is (= 10 wx))
      (is (= -4 wy)))

    (let [{x :x y :y wx :wx wy :wy} (take-evasive-waypoint-actions [[\F 10]
                                                                    [\N 3]
                                                                    [\F 7]
                                                                    [\R 90]])]
      (is (= 170 x))
      (is (= -38 y))
      (is (= 4 wx))
      (is (= 10 wy)))))
