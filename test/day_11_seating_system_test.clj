(ns day-11-seating-system-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-11-seating-system :refer
    [->seatings
     quiesce-seatings
     update-seatings
     update-seatings-fov
     num-occupied]]))

(def example-seatings-initial [[\L \. \L \L \. \L \L \. \L \L]
                               [\L \L \L \L \L \L \L \. \L \L]
                               [\L \. \L \. \L \. \. \L \. \.]
                               [\L \L \L \L \. \L \L \. \L \L]
                               [\L \. \L \L \. \L \L \. \L \L]
                               [\L \. \L \L \L \L \L \. \L \L]
                               [\. \. \L \. \L \. \. \. \. \.]
                               [\L \L \L \L \L \L \L \L \L \L]
                               [\L \. \L \L \L \L \L \L \. \L]
                               [\L \. \L \L \L \L \L \. \L \L]])

(def example-seatings-first [[\# \. \# \# \. \# \# \. \# \#]
                             [\# \# \# \# \# \# \# \. \# \#]
                             [\# \. \# \. \# \. \. \# \. \.]
                             [\# \# \# \# \. \# \# \. \# \#]
                             [\# \. \# \# \. \# \# \. \# \#]
                             [\# \. \# \# \# \# \# \. \# \#]
                             [\. \. \# \. \# \. \. \. \. \.]
                             [\# \# \# \# \# \# \# \# \# \#]
                             [\# \. \# \# \# \# \# \# \. \#]
                             [\# \. \# \# \# \# \# \. \# \#]])

(def example-seatings-second [[\# \. \# \L \. \L \# \. \# \#]
                              [\# \L \L \L \# \L \L \. \L \#]
                              [\L \. \# \. \L \. \. \# \. \.]
                              [\# \L \# \# \. \# \# \. \L \#]
                              [\# \. \# \L \. \L \L \. \L \L]
                              [\# \. \# \L \# \L \# \. \# \#]
                              [\. \. \L \. \L \. \. \. \. \.]
                              [\# \L \# \L \# \# \L \# \L \#]
                              [\# \. \L \L \L \L \L \L \. \L]
                              [\# \. \# \L \# \L \# \. \# \#]])

(def example-seatings-final [[\# \. \# \L \. \L \# \. \# \#]
                             [\# \L \L \L \# \L \L \. \L \#]
                             [\L \. \# \. \L \. \. \# \. \.]
                             [\# \L \# \# \. \# \# \. \L \#]
                             [\# \. \# \L \. \L \L \. \L \L]
                             [\# \. \# \L \# \L \# \. \# \#]
                             [\. \. \L \. \L \. \. \. \. \.]
                             [\# \L \# \L \# \# \L \# \L \#]
                             [\# \. \L \L \L \L \L \L \. \L]
                             [\# \. \# \L \# \L \# \. \# \#]])

(defn ->xss-vec [xss]
  (vec (map vec xss)))

(deftest ->seatings-test
  (testing "parse raw input"
    (is (= example-seatings-initial
           (->xss-vec (->seatings "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"))))))

(deftest update-seatings-test
  (testing "seatings update correctly"
    (is (= example-seatings-first
           (->xss-vec (update-seatings example-seatings-initial))))))

(deftest quiesce-seatings-test
  (testing "halts updating seats correctly"
    (is (= example-seatings-final
           (->xss-vec (quiesce-seatings example-seatings-initial))))))

(def example-seatings-first-fov [[\# \. \# \# \. \# \# \. \# \#]
                                 [\# \# \# \# \# \# \# \. \# \#]
                                 [\# \. \# \. \# \. \. \# \. \.]
                                 [\# \# \# \# \. \# \# \. \# \#]
                                 [\# \. \# \# \. \# \# \. \# \#]
                                 [\# \. \# \# \# \# \# \. \# \#]
                                 [\. \. \# \. \# \. \. \. \. \.]
                                 [\# \# \# \# \# \# \# \# \# \#]
                                 [\# \. \# \# \# \# \# \# \. \#]
                                 [\# \. \# \# \# \# \# \. \# \#]])

(def example-seatings-second-fov [[\# \. \L \L \. \L \L \. \L \#]
                                  [\# \L \L \L \L \L \L \. \L \L]
                                  [\L \. \L \. \L \. \. \L \. \.]
                                  [\L \L \L \L \. \L \L \. \L \L]
                                  [\L \. \L \L \. \L \L \. \L \L]
                                  [\L \. \L \L \L \L \L \. \L \L]
                                  [\. \. \L \. \L \. \. \. \. \.]
                                  [\L \L \L \L \L \L \L \L \L \#]
                                  [\# \. \L \L \L \L \L \L \. \L]
                                  [\# \. \L \L \L \L \L \. \L \#]])

(def example-seatings-final-fov [[\# \. \L \# \. \L \# \. \L \#]
                                 [\# \L \L \L \L \L \L \. \L \L]
                                 [\L \. \L \. \L \. \. \# \. \.]
                                 [\# \# \L \# \. \# \L \. \L \#]
                                 [\L \. \L \# \. \L \L \. \L \#]
                                 [\# \. \L \L \L \L \# \. \L \L]
                                 [\. \. \# \. \L \. \. \. \. \.]
                                 [\L \L \L \# \# \# \L \L \L \#]
                                 [\# \. \L \L \L \L \L \# \. \L]
                                 [\# \. \L \# \L \L \# \. \L \#]])


(deftest update-seatings-fov-test
  (testing "seatings update correctly with fov"

    (is (= example-seatings-first-fov
           (->xss-vec (update-seatings-fov example-seatings-initial))))

    (is (= example-seatings-second-fov
           (->xss-vec (update-seatings-fov example-seatings-first-fov))))))

(deftest solve-quiesce-seatings-fov-test
  (testing "halts updating seats with fov update correctly"
    (let [actual-final-seatings-fov (quiesce-seatings example-seatings-initial
                                                      :update-fn update-seatings-fov)]
      (is (= example-seatings-final-fov (->xss-vec actual-final-seatings-fov)))
      (is (= 26 (num-occupied actual-final-seatings-fov))))))
