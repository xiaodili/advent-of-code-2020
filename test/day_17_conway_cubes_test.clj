(ns day-17-conway-cubes-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-17-conway-cubes :as imp]))

(deftest next-configuration-test
  (testing "state changes correctly"
    (let [initial-configuration [[[\. \# \.]
                                  [\. \. \#]
                                  [\# \# \#]]]
          expected-next-configuration [[[\# \. \.]
                                        [\. \. \#]
                                        [\. \# \.]]

                                       [[\# \. \#]
                                        [\. \# \#]
                                        [\. \# \.]]

                                       [[\# \. \.]
                                        [\. \. \#]
                                        [\. \# \.]]]]
      (is (= expected-next-configuration (imp/next-configuration initial-configuration))))))
