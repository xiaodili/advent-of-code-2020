(ns day-05-binary-boarding-test
  (:require
   [day-05-binary-boarding :refer
    [partition-up
     partition-down
     ->row
     ->column
     ->seat-id]]
   [clojure.test :refer [testing deftest is]]))

(deftest partition-test
  (testing "partitioning logic"
    (is (= [4 7] (partition-up 0 7)))
    (is (= [6 7] (partition-up 4 7)))
    (is (= [7 7] (partition-up 6 7)))
    (is (= [7 7] (partition-up 6 7)))
    (is (= [0 3] (partition-down 0 7)))
    (is (= [0 1] (partition-down 0 3)))
    (is (= [0 0] (partition-down 0 1)))
    (is (= [4 5] (partition-down 4 7)))
    ))

(deftest ->row-test
  (testing "finding the row"
    (is (= 44 (->row "FBFBBFF")))))

(deftest ->column-test
  (testing "finding the column"
    (is (= 5 (->column "RLR")))))

(deftest ->seat-id-test
  (testing "getting the seat-id"
    (is (= 357 (->seat-id "FBFBBFFRLR")))))
