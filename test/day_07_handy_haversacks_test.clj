(ns day-07-handy-haversacks-test
  (:require
   [clojure.test :refer [testing deftest is]]
   [day-07-handy-haversacks :refer
    [->rule
     ->rules
     ->rules-lookup
     traversable-to
     num-bags-in]]))

(deftest ->rule-test
  (testing "parse a rule line with edges"
    (let [rule-line "light red bags contain 1 bright white bag, 2 muted yellow bags."
          expected-rule ["light red" [[1 "bright white"] [2 "muted yellow"]]]]
      (is (= expected-rule (->rule rule-line)))))

  (testing "parse a rule line with one edge"
    (let [rule-line "bright white bags contain 1 shiny gold bag."
          expected-rule ["bright white" [[1 "shiny gold"]]]]
      (is (= expected-rule (->rule rule-line)))))

  (testing "parse a rule line without edges"
    (let [rule-line "faded blue bags contain no other bags."
          expected-rule ["faded blue" []]]
      (is (= expected-rule (->rule rule-line))))))

(def sample-rules [["light red" [[1 "bright white"] [2 "muted yellow"]]]
                   ["dark orange" [[3 "bright white"] [4 "muted yellow"]]]
                   ["bright white" [[1 "shiny gold"]]]
                   ["muted yellow" [[2 "shiny gold"] [9 "faded blue"]]]
                   ["shiny gold" [[1 "dark olive"] [2 "vibrant plum"]]]
                   ["dark olive" [[3 "faded blue"] [4 "dotted black"]]]
                   ["vibrant plum" [[5 "faded blue"] [6 "dotted black"]]]
                   ["faded blue" []]
                   ["dotted black" []]])

(deftest ->rules-test
  (testing "parse the rules into a list of relationships"
    (let [raw-input "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."
          expected-rules sample-rules]
      (is (= expected-rules (->rules raw-input))))))

(def sample-rules-lookup {"muted yellow" ["dark orange" "light red"]
                                 "bright white" ["dark orange" "light red"]
                                 "shiny gold" ["muted yellow" "bright white"]
                                 "faded blue" ["vibrant plum" "dark olive" "muted yellow"]
                                 "vibrant plum" ["shiny gold"]
                                 "dark olive" ["shiny gold"]
                                 "dotted black" ["vibrant plum" "dark olive"]})

(deftest ->rules-lookup-test
  (testing "can create an intermediate reverse bag colour lookup map from a list of parsed rules"
    (let [expected-rules-lookup sample-rules-lookup]
      (is (= expected-rules-lookup (->rules-lookup sample-rules))))))

(deftest traversable-to-test
  (testing "returns a list of traversable bags, given a rules-lookup and starting bag colour"
    (is (= 4 (traversable-to sample-rules-lookup "shiny gold")))))

(deftest num-bags-in-test
  (testing "number of bags in starting bag"
    (is (= 0 (num-bags-in sample-rules "faded blue")))
    (is (= 11 (num-bags-in sample-rules "vibrant plum")))
    (is (= 32 (num-bags-in sample-rules "shiny gold")))))
