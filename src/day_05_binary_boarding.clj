(ns day-05-binary-boarding
  (:require
   [clojure.string :as str]))

(defn partition-up [lower upper]
  [(+ lower (/ (inc (- upper lower)) 2)) upper])

(defn partition-down [lower upper]
  [lower (dec (+ (/ (inc (- upper lower)) 2) lower))])

(defn ->row [row-str]
  (with-local-vars [bounds [0 127]]
    (doseq [instruction row-str]
      (var-set bounds (if (= instruction \F) ;; or \B
                        (apply partition-down @bounds)
                        (apply partition-up @bounds))))
    (first @bounds)))

(defn ->column [column-str]
  (with-local-vars [bounds [0 7]]
    (doseq [instruction column-str]
      (var-set bounds (if (= instruction \L) ;; or \R
                        (apply partition-down @bounds)
                        (apply partition-up @bounds))))
    (first @bounds)))

(defn ->seat-id [boarding-str]
  (let [row-str (subs boarding-str 0 7)
        column-str (subs boarding-str 7)
        row (->row row-str)
        column (->column column-str)]
    (+ (* row 8) column)))

(defn ->seat-ids [raw-input]
  (let [boarding-passes (str/split-lines raw-input)]
    (map ->seat-id boarding-passes)))

(defn solve-1 [raw-input]
  (apply max (->seat-ids raw-input)))

(defn solve-2 [raw-input]
  (let [seat-ids (->seat-ids raw-input)
        seat-ids-sorted-array (to-array (sort seat-ids))]

    (letfn [(next-seat-free? [seat-idx]
              (let [prev-seat-id (aget seat-ids-sorted-array (dec seat-idx))
                    next-seat-id (aget seat-ids-sorted-array (inc seat-idx))
                    curr-seat-id (aget seat-ids-sorted-array seat-idx)]
                (when (and (= prev-seat-id (dec curr-seat-id)) (= (+ curr-seat-id 2) next-seat-id))
                  (inc curr-seat-id))))]

      (some next-seat-free?
            (range 1 (dec (count seat-ids)))))))
