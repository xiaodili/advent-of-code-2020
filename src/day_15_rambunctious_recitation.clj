(ns day-15-rambunctious-recitation
  (:require
   [clojure.string :as str]))

(defn ->numbers [raw-input]
  ;; Make sure this is a vec for conj to work
  ;; https://stackoverflow.com/a/17910712
  (vec (map read-string (str/split raw-input #","))))

(defn next-number [numbers]
  (let [idx (.lastIndexOf (drop-last numbers) (last numbers))]
    (conj numbers 
          (if (= idx -1)
            0
            (- (count numbers) idx 1)))))

(defn number-at-turn [starting-numbers turn]
  (let [num-starting-numbers (count starting-numbers)
        num-to-generate (inc (- turn num-starting-numbers))]

    (if (<= turn num-starting-numbers)
      (nth starting-numbers (dec turn))
      (last 
       (last
        (take num-to-generate
              (iterate next-number starting-numbers)))))))

(defn solve-1 [raw-input]
  (let [starting-numbers (->numbers raw-input)]
    (number-at-turn starting-numbers 2020)))

(defn number-at-turn-faster [starting-numbers target-turn]
  (let [num-starting-numbers (count starting-numbers)
        starting-last-idx-lookup (into {} (map vector (drop-last starting-numbers) (range)))]

    (with-local-vars [last-number (last starting-numbers),
                      last-idx-lookup starting-last-idx-lookup]

      (doseq [current-turn (range (inc num-starting-numbers) (inc target-turn))]

        (let [new-number (if (contains? @last-idx-lookup @last-number)
                           (- (dec current-turn) (inc (get @last-idx-lookup @last-number)))
                           0)]
          (var-set last-idx-lookup (assoc @last-idx-lookup @last-number (- current-turn 2)))
          (var-set last-number new-number)))

      @last-number)))

(defn solve-2 [raw-input]
  (let [starting-numbers (->numbers raw-input)]
    (number-at-turn-faster starting-numbers 30000000)))
