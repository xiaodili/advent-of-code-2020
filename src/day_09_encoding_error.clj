(ns day-09-encoding-error
  (:require
   [clojure.string :as str]
   [clojure.math.combinatorics :refer [combinations]]))

(defn cross-sums [numbers]
  (map #(apply + %)
       (combinations numbers 2)))

(defn first-invalid-xmas-number [port-numbers preamble-length]
  (let [[preamble payload] (split-at preamble-length port-numbers)]
    (with-local-vars [valid-sums (cross-sums preamble)]
      (letfn [(valid-number? [[idx number]]
                (if (not (some #{number} @valid-sums))
                  number
                  (do
                    (let [next-idx (inc idx)
                          end-idx (+ next-idx preamble-length)]
                      (var-set valid-sums (cross-sums (subvec port-numbers next-idx end-idx))))
                    nil)))]
        (some valid-number? (map-indexed list payload))))))

(defn ->numbers [raw-input]
  (into [] (map read-string (str/split-lines raw-input))))

(defn solve-1 [raw-input]
  (let [port-numbers (->numbers raw-input)]
    (first-invalid-xmas-number port-numbers 25)))

;; https://codility.com/media/train/13-CaterpillarMethod.pdf (AKA general method of 2 pointers)
(defn contiguous-sequence [numbers target]
  (let [n (count numbers)]
    (with-local-vars [front 0, total 0]
      (letfn [(meets-target [back]
                (while (and
                        (< @front n)
                        (<= (+ @total (nth numbers @front)) target))
                  (do
                    (var-set total (+ @total (nth numbers @front)))
                    (var-set front (inc @front))))
                (if (= @total target)
                  (subvec numbers back @front)
                  (do
                    (var-set total (- @total (nth numbers back)))
                    nil)))]
        (some meets-target (range n))))))

(defn solve-2 [raw-input]
  (let [port-numbers (->numbers raw-input)
        invalid-number (first-invalid-xmas-number port-numbers 25)
        sum-sequence (contiguous-sequence port-numbers invalid-number)]
    (+ (apply min sum-sequence) (apply max sum-sequence))))
