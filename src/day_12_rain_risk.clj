(ns day-12-rain-risk
  (:require
   [clojure.string :as str]))

(defn ->actions [raw-input]
  (map #(let [action (first %)
              value (read-string (subs % 1))]
          [action value])
       (str/split-lines raw-input)))

(defn rotate [orientation rotation sign]
  (mod
   (+ 360
      (+ orientation (* sign rotation)))
   360))

;; polar coords but reflected on xaxis
(defn apply-action [state action]
  (let [[instruction value] action]
    (cond
      (= instruction \N) (assoc state :y (- (:y state) value))
      (= instruction \S) (assoc state :y (+ (:y state) value))
      (= instruction \E) (assoc state :x (+ (:x state) value))
      (= instruction \W) (assoc state :x (- (:x state) value))
      (= instruction \L) (assoc state :dir (rotate (:dir state) value 1))
      (= instruction \R) (assoc state :dir (rotate (:dir state) value -1))
      (= instruction \F) (cond
                           (= (:dir state) 0) (assoc state :x (+ (:x state) value))
                           (= (:dir state) 90) (assoc state :y (- (:y state) value))
                           (= (:dir state) 180) (assoc state :x (- (:x state) value))
                           (= (:dir state) 270) (assoc state :y (+ (:y state) value))))))

(defn take-evasive-actions [actions & {:keys [x y dir]
                                       :or {x 0
                                            y 0
                                            dir 0}}]
  (reduce apply-action
          {:x x
           :y y
           :dir dir}
          actions))

(defn manhattan-distance [x y]
  (+ (Math/abs x) (Math/abs y)))

(defn solve-1 [raw-input]
  (let [actions (->actions raw-input)
        {x :x y :y} (take-evasive-actions actions)]
    (manhattan-distance x y)))

(defn cos-degrees [degrees]
  (int (Math/cos (Math/toRadians degrees))))

(defn sin-degrees [degrees]
  (int (Math/sin (Math/toRadians degrees))))

;; polar coords but reflected on xaxis
(defn rotate-waypoint [state instruction degrees]
  (let [{wx :wx wy :wy} state]
    (if (= instruction \R)
      ;;clockwise (R)
      (assoc state
             :wx (- (* wx (cos-degrees degrees))
                    (* wy (sin-degrees degrees)))
             :wy (+ (* wx (sin-degrees degrees))
                    (* wy (cos-degrees degrees))))
      ;; counterclockwise (L)
      (assoc state
             :wx (+ (* wx (cos-degrees degrees))
                    (* wy (sin-degrees degrees)))
             :wy (- (* wy (cos-degrees degrees))
                    (* wx (sin-degrees degrees)))))))

(defn apply-waypoint-action [state action]
  (let [[instruction value] action]
    (cond
      (= instruction \N) (assoc state :wy (- (:wy state) value))
      (= instruction \S) (assoc state :wy (+ (:wy state) value))
      (= instruction \E) (assoc state :wx (+ (:wx state) value))
      (= instruction \W) (assoc state :wx (- (:wx state) value))
      (or (= instruction \R)
          (= instruction \L)) (rotate-waypoint state instruction value)
      (= instruction \F) (assoc state
                                :x (+ (:x state) (* (:wx state) value))
                                :y (+ (:y state) (* (:wy state) value))))))

(defn take-evasive-waypoint-actions [actions & {:keys [x y dir wx wy]
                                                :or {x 0
                                                     y 0
                                                     dir 0
                                                     wx 10
                                                     wy -1}}]
  (reduce apply-waypoint-action
          {:x x
           :y y
           :dir dir
           :wx wx
           :wy wy}
          actions))

(defn solve-2 [raw-input]
  (let [actions (->actions raw-input)
        {x :x y :y} (take-evasive-waypoint-actions actions)]
    (manhattan-distance x y)))
