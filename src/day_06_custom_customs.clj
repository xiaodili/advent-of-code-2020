(ns day-06-custom-customs
  (:require
   [clojure.set :refer [intersection]]
   [clojure.string :as str]))

(defn contains-answers? [line]
  (not (str/blank? line)))

(defn ->group-answerss [raw-input]
  (with-local-vars [group-answerss nil, current-group []]
    (doseq [answers-maybe (str/split-lines raw-input)]
      (if (contains-answers? answers-maybe)
        (var-set current-group (cons answers-maybe @current-group))
        (do
          (var-set group-answerss (cons (reverse @current-group) @group-answerss))
          (var-set current-group []))))
    (var-set group-answerss (cons (reverse @current-group) @group-answerss))
    (reverse @group-answerss)))

(defn count-yes [group-answers]
  (count (set (str/join group-answers))))

(defn count-all-yes [group-answers]
  (count (apply intersection (map set group-answers))))

(defn solve-1 [raw-input]
  (let [group-answerss (->group-answerss raw-input)
        num-yess (map count-yes group-answerss)]
    (apply + num-yess)))

(defn solve-2 [raw-input]
  (let [group-answerss (->group-answerss raw-input)
        num-all-yess (map count-all-yes group-answerss)]
    (apply + num-all-yess)))
