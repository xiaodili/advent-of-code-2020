(ns runner
  (:require
   [day-01-report-repair :as day-01]
   [day-02-password-philosophy :as day-02]
   [day-03-toboggan-trajectory :as day-03]
   [day-04-passport-processing :as day-04]
   [day-05-binary-boarding :as day-05]
   [day-06-custom-customs :as day-06]
   [day-07-handy-haversacks :as day-07]
   [day-08-handheld-halting :as day-08]
   [day-09-encoding-error :as day-09]
   [day-10-adapter-array :as day-10]
   [day-11-seating-system :as day-11]
   [day-12-rain-risk :as day-12]
   [day-13-shuttle-search :as day-13]
   [day-14-docking-data :as day-14]
   [day-15-rambunctious-recitation :as day-15]
   [day-16-ticket-translation :as day-16]
   [day-17-conway-cubes :as day-17]
   [day-18-operation-order :as day-18]
   [day-19-monster-messages :as day-19]
   [day-20-jurassic-jigsaw :as day-20])

  (:gen-class))

(defn -main [puzzle-str raw-input-path]

  (let [puzzles {"1-1" day-01/solve-1
                 "1-2" day-01/solve-2
                 "2-1" day-02/solve-1
                 "2-2" day-02/solve-2
                 "3-1" day-03/solve-1
                 "3-2" day-03/solve-2
                 "4-1" day-04/solve-1
                 "4-2" day-04/solve-2
                 "5-1" day-05/solve-1
                 "5-2" day-05/solve-2
                 "6-1" day-06/solve-1
                 "6-2" day-06/solve-2
                 "7-1" day-07/solve-1
                 "7-2" day-07/solve-2
                 "8-1" day-08/solve-1
                 "8-2" day-08/solve-2
                 "9-1" day-09/solve-1
                 "9-2" day-09/solve-2
                 "10-1" day-10/solve-1
                 "10-2" day-10/solve-2
                 "11-1" day-11/solve-1
                 "11-2" day-11/solve-2
                 "12-1" day-12/solve-1
                 "12-2" day-12/solve-2
                 "13-1" day-13/solve-1
                 "13-2" day-13/solve-2
                 "14-1" day-14/solve-1
                 "14-2" day-14/solve-2
                 "15-1" day-15/solve-1
                 "15-2" day-15/solve-2
                 "16-1" day-16/solve-1
                 "16-2" day-16/solve-2
                 "17-1" day-17/solve-1
                 "17-2" day-17/solve-2
                 "18-1" day-18/solve-1
                 "18-2" day-18/solve-2
                 "19-1" day-19/solve-1
                 "19-2" day-19/solve-2
                 "20-1" day-20/solve-1
                 "20-2" day-20/solve-2}
        solve-puzzle (puzzles puzzle-str (fn [_] (str "Invalid puzzle: " puzzle-str)))]

    (println (solve-puzzle (slurp raw-input-path)))))
