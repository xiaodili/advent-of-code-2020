(ns day-08-handheld-halting
  (:require
   [clojure.string :as str]
   [flatland.ordered.set :refer
    [ordered-set]]))

(defn ->instruction [instruction-str]
  (let [[operation argument-str] (str/split instruction-str #" ")]
    [operation (read-string argument-str)]))

(defn ->instructions [raw-input]
  (map ->instruction (str/split-lines raw-input)))

(defn run-with-loop-detect [instructions]
  (with-local-vars [accumulator 0,
                    current-instruction-ptr 0,
                    visited #{}]
    (let [program (to-array instructions)]
      (letfn [(handle-nop []
                (inc @current-instruction-ptr))
              (handle-acc [arg]
                (var-set accumulator (+ @accumulator arg))
                (inc @current-instruction-ptr))
              (handle-jmp [arg]
                (+ @current-instruction-ptr arg))]

        (while (not (contains? @visited @current-instruction-ptr))
          (let [[operation argument] (aget program @current-instruction-ptr)
                next-instruction-ptr (cond
                                   (= operation "nop") (handle-nop)
                                   (= operation "acc") (handle-acc argument)
                                   (= operation "jmp") (handle-jmp argument))]
            (var-set visited (conj @visited @current-instruction-ptr))
            (var-set current-instruction-ptr next-instruction-ptr)))

        @accumulator))))

(defn solve-1 [raw-input]
  (run-with-loop-detect (->instructions raw-input)))

(defn elems-from [xs y]
  (let [y-idx (.indexOf xs y)]
    (subvec xs y-idx)))

;; Returns either:
;; * The encountered loop path
;; * The value of the accumulator if program terminates
(defn run-program [instructions]
  (with-local-vars [accumulator 0,
                    current-instruction-ptr 0,
                    visited (ordered-set)]
      (letfn [(handle-nop []
                (inc @current-instruction-ptr))

              (handle-acc [arg]
                (var-set accumulator (+ @accumulator arg))
                (inc @current-instruction-ptr))

              (handle-jmp [arg]
                (+ @current-instruction-ptr arg))

              (entering-loop? []
                (contains? @visited @current-instruction-ptr))

              (terminated? []
                (= @current-instruction-ptr (count instructions)))]

        (while (and (not (terminated?)) (not (entering-loop?)))

          (let [[operation argument] (nth instructions @current-instruction-ptr)
                next-instruction-ptr (cond
                                   (= operation "nop") (handle-nop)
                                   (= operation "acc") (handle-acc argument)
                                   (= operation "jmp") (handle-jmp argument))]

            (var-set visited (conj @visited @current-instruction-ptr))
            (var-set current-instruction-ptr next-instruction-ptr)))

        (if (terminated?)
          @accumulator
          (elems-from (into [] @visited) @current-instruction-ptr)))))

(defn solve-2 [raw-input]
  (let [instructions (into [] (->instructions raw-input))
        loop-indices (run-program instructions)
        jmp-nop-indices (filter #(let [operation (first (nth instructions %))]
                                   (or (= operation "jmp") (= operation "nop")))
                                loop-indices)]
    (letfn [(modify-instructions [idx]
              (let [[operation argument] (nth instructions idx)
                    new-operation (if (= operation "nop") "jmp" "nop")]
                (assoc instructions idx [new-operation argument])))]

      (some #(let [result (run-program (modify-instructions %))]
               (when (number? result)
                 result)) jmp-nop-indices))))
