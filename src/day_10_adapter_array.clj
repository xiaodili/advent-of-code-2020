(ns day-10-adapter-array
  (:require
   [clojure.string :as str]))

(defn ->chain [adapters overlap]
  (let [adapters-sorted (sort adapters)
        highest-adapter (last adapters-sorted)
        all-jolts-sorted (concat [0]
                                 adapters-sorted
                                 [(+ highest-adapter overlap)])]
    all-jolts-sorted))

(defn ->differences [numbers]
  (map #(- %1 %2)
       (rest numbers)
       (drop-last numbers)))

(defn ->adapters [raw-input]
  (map read-string (str/split-lines raw-input)))

(defn solve-1 [raw-input]
  (let [adapters (->adapters raw-input)
        difference-counts (frequencies
                           (->differences
                            (->chain adapters 3)))]
    (* (get difference-counts 3 0)
       (get difference-counts 1 0))))

(defn num-arrangements [adapters overlap]
  (let [chain (->chain adapters overlap)
        chain-set (set chain)]
    (with-local-vars [memo {-2 0
                            -1 0
                            0 1}]
      (letfn [(num-arrangements-memoised [adapter-num]
                (if (contains? chain-set adapter-num)
                  (do
                    (when (not (contains? @memo adapter-num))
                      (let [result (apply +
                                          (map
                                           #(num-arrangements-memoised (- adapter-num % 1))
                                           (range overlap)))]
                        (var-set memo (assoc @memo adapter-num result))))
                    (get @memo adapter-num))
                  0))]
        (num-arrangements-memoised (last chain))))))

(defn solve-2 [raw-input]
  (let [adapters (->adapters raw-input)]
    (num-arrangements adapters 3)))
