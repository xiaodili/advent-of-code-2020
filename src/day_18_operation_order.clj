(ns day-18-operation-order
  (:require
   [clojure.string :as str]))

(defn parse-exp [expression-str]

  (with-local-vars [char-ptr 0]

    (letfn [(consume [stack _]

              (if (= @char-ptr (count expression-str))

                (reduced stack)
                
                (let [c (nth expression-str @char-ptr)]
                  (var-set char-ptr (inc @char-ptr))
                  (cond
                    (= c \space) stack
                    (= c \() (conj stack (reduce consume [] (range)))
                    (= c \)) (reduced stack)
                    ;; Don't need to deal with multiple length integers (yet)
                    :else (conj stack (if (or (= c \*) (= c \+))
                                        c
                                        (Integer/parseInt (str c))))))))]

      (reduce consume [] (range)))))

(defn eval-sexp [sexp]

  (if (not (coll? sexp))

    ;; atom
    sexp

    ;; sexpression
    (with-local-vars [remaining-sexps sexp]

      (while (not= (count @remaining-sexps) 1)

        (let [[left-form operator right-form & _] @remaining-sexps
              left-val (eval-sexp left-form)
              right-val (eval-sexp right-form)
              applied-val (cond
                            (= operator \*) (* left-val right-val)
                            (= operator \+) (+ left-val right-val))]
          (var-set remaining-sexps (cons
                                    applied-val
                                    (nthrest @remaining-sexps 3)))))
      (first @remaining-sexps))))

(defn evaluate [expression-str]
  (eval-sexp (parse-exp expression-str)))

(defn solve-1 [raw-input]
  (let [exps (str/split-lines raw-input)
        evaleds (map evaluate exps)]
    (apply + evaleds)))

(defn add-idx-in-sexp? [sexp]
  (.indexOf sexp \+))

(defn eval-sexp-advanced [sexp]

  (if (not (coll? sexp))

    ;; atom
    sexp

    (with-local-vars [remaining-sexps sexp]

      (while (not= (count @remaining-sexps) 1)

        (let [add-idx-maybe (add-idx-in-sexp? @remaining-sexps)]

          (if (= -1 add-idx-maybe)
            
            ;; Take the first three sexps
            (let [[left-form operator right-form & _] @remaining-sexps
                  left-val (eval-sexp-advanced left-form)
                  right-val (eval-sexp-advanced right-form)
                  applied-val (cond
                                (= operator \*) (* left-val right-val)
                                (= operator \+) (+ left-val right-val))]
              (var-set remaining-sexps (cons
                                        applied-val
                                        (nthrest @remaining-sexps 3))))

            ;; Otherwise do the addition bit first
            (let [add-idx add-idx-maybe
                  left-form (nth @remaining-sexps (dec add-idx))
                  right-form (nth @remaining-sexps (inc add-idx))
                  left-val (eval-sexp-advanced left-form)
                  right-val (eval-sexp-advanced right-form)
                  applied-val (+ left-val right-val)]

              (var-set remaining-sexps (concat
                                        (take (dec add-idx) @remaining-sexps)
                                        [applied-val]
                                        (nthrest @remaining-sexps (+ add-idx 2))))))))

      (first @remaining-sexps))))

(defn evaluate-advanced [expression-str]
  (eval-sexp-advanced (parse-exp expression-str)))

(defn solve-2 [raw-input]
  (let [exps (str/split-lines raw-input)
        evaleds (map evaluate-advanced exps)]
    (apply + evaleds)))
