(ns day-13-shuttle-search
  (:require
   [clojure.string :as str]
   ))

(defn ->notes [raw-input]
  (let [[earliest bus-ids] (str/split-lines raw-input)]
    [(read-string earliest)
     (map read-string 
          (str/split bus-ids #","))]))

(defn ->wait-time [timestamp bus-id]
  (let [leftover (mod timestamp bus-id)]
    (if (zero? leftover)
      leftover
      (- bus-id leftover))))

(defn earliest-bus [earliest-timestamp bus-ids]
  ;; Returns [bus-id wait-time]
  (let [bus-id-wait-times (map
                           #(vector % (->wait-time earliest-timestamp %))
                           bus-ids)]
    (apply min-key second bus-id-wait-times)))

(defn solve-1 [raw-input]
  (let [[earliest-timestamp bus-ids] (->notes raw-input)
        bus-ids-in-service (filter int? bus-ids)
        [bus-id wait-time] (earliest-bus earliest-timestamp bus-ids-in-service)]
    (* bus-id wait-time)))

(defn first-multiple [start ts]
  (let [quotient (quot start ts)]
    (* ts (inc quotient))))

;; Naive solution
#_(defn perfect-departure [bus-ids]
  (let [zero-wait-time-ts (first bus-ids)
        bus-id-wait-times (map-indexed vector bus-ids)]

    (letfn [(perfect-departure? [timestamp]
              (every? #(let [[req-wait-time bus-id] %]
                         (or (= bus-id 'x)
                             (= (->wait-time timestamp bus-id) req-wait-time)))
                      bus-id-wait-times))]

      (with-local-vars [timestamp (first-multiple
                                   100000000000000
                                   ;; 100000
                                   zero-wait-time-ts)]
      ;; (with-local-vars [timestamp zero-wait-time-ts]

        (while (not (perfect-departure? @timestamp))
          (var-set timestamp (+ @timestamp zero-wait-time-ts)))

        @timestamp))))


;; http://mathforum.org/library/drmath/view/75030.html
(defn perfect-departure [bus-ids]
  (let [start (first bus-ids)
        wait-time-bus-ids (vec (filter
                                #(int? (second %))
                                (map-indexed vector bus-ids)))]

    (with-local-vars [timestamp start,
                      lcm-step start,
                      next-fit 1]

      (while (not (= @next-fit (count wait-time-bus-ids)))
        (let [[wait-time bus-id] (nth wait-time-bus-ids @next-fit)]

          (while (not (= (mod (+ @timestamp wait-time) bus-id) 0))
            (var-set timestamp (+ @timestamp @lcm-step)))

          (var-set lcm-step (* @lcm-step bus-id))
          (var-set next-fit (inc @next-fit))))

      @timestamp)))

(defn solve-2 [raw-input]
  (let [[_ bus-ids] (->notes raw-input)]
    (perfect-departure bus-ids)))
