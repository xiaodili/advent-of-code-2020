;; Encapsulation trawl
;; https://stackoverflow.com/questions/17725224/how-to-encapsulate-in-clojure
;; https://stackoverflow.com/questions/5554178/can-i-create-mutable-state-inside-clojure-records
;; https://stackoverflow.com/questions/11958027/clojures-defrecord-how-to-use-it

(defn Blah [start-val]
  (with-local-vars [start start-val]
    (letfn [(get-val [] @start)
            (set-val [val] (var-set start val))]
      {:get-val get-val
       :set-val set-val})))

(def test-closure (Blah 123))

((:get-val test-closure))
;; #object[clojure.lang.Var$Unbound 0x4c8c713e "Unbound: #<Var: --unnamed-->"]

;; Seems like we gotta do it this way
(defn Blah2 [start-val]
  (let [start (atom start-val)]
    (letfn [(get-val [] @start)
            (set-val [val] (reset! start val))]
      {:get-val get-val
       :set-val set-val})))

(def test-closure2 (Blah2 123))

((:get-val test-closure2))
;; 123

;; Or with records?
(defrecord MyType [start])

(def my-type (->MyType 123))

(assoc my-type :start 1236)
;; #day_07_handy_haversacks.MyType{:start 1236}

my-type
;; #day_07_handy_haversacks.MyType{:start 123}

;; Cool...

;; Symbols

(def raw-input "939
7,13,x,x,59,x,31,19")

(map type
     (rest (->notes raw-input)))

(second (->notes raw-input))
;; (7 13 x x 59 x 31 19)

(nth
 (second (->notes raw-input))
 2)
;; x

(type
 (nth
  (second (->notes raw-input))
  2))
;; clojure.lang.Symbol

(= 'x
   (nth
    (second (->notes raw-input))
    2))
;; true
