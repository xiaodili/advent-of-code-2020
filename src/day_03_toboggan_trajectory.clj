(ns day-03-toboggan-trajectory
  (:require
   [clojure.string :as str]))

(defn ->char-list [string]
  (seq (char-array string)))

(defn parse-input [raw-input]
  (to-array-2d
   (map ->char-list
        (str/split-lines raw-input))))

(defn traverse [tree-map velocity]
  (with-local-vars [current-path nil, x 0, y 0]
    (let [height (count tree-map)
          width (count (first tree-map))
          [x-vel y-vel] velocity]

      (while (< @y height)
        (var-set current-path (cons (aget tree-map @y @x) @current-path))
        (var-set y (+ @y y-vel))
        (var-set x (mod (+ @x x-vel) width)))

      (reverse @current-path))))

(defn num-trees-encountered [tree-map velocity]
  (count (filter #(= % \#) (traverse tree-map velocity))))

(defn solve-1 [raw-input]
  (let [tree-map (parse-input raw-input)]
    (num-trees-encountered tree-map [3 1])))

(defn solve-2 [raw-input]
  (let [tree-map (parse-input raw-input)
        velocities [[1 1]
                    [3 1]
                    [5 1]
                    [7 1]
                    [1 2]]]
    (apply * (map #(num-trees-encountered tree-map %) velocities))))
