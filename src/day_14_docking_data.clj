(ns day-14-docking-data
  (:require
   [clojure.pprint :refer (cl-format)]
   [clojure.string :as str]
   [clojure.math.combinatorics :refer [cartesian-product]]))

(defn ->mask-instruction [mask-str]
  (second (str/split mask-str #" = ")))

(defn ->write-instruction [write-str]
  (map read-string
       (rest (re-matches #"mem\[(\d+)\] = (\d+)" write-str))))

(defn parse-input [raw-input]
  (let [instruction-strs (str/split-lines raw-input)]
    (map #(if (str/starts-with? % "mask = ")
            (->mask-instruction %)
            (->write-instruction %)) instruction-strs)))

(defn ->number [binary-str]
  ;; Too big :(
  ;; (apply + 
  ;;        (map #(if (= %1 \1)
  ;;                (int (Math/pow 2 %2))
  ;;                0)
  ;;             (reverse binary-str) (range)))
  (BigInteger. binary-str 2)) 

(defn run-instruction [value mask]
  (let [binary-str (apply
                    str
                    (map #(if (not= %1 \X)
                            %1 %2)
                         mask
                         (cl-format nil "~36,'0',B" value)))]
    binary-str))

(defn mask-instruction? [instruction]
  (string? instruction))

(defn solve-1 [raw-input]
  (let [instructions (parse-input raw-input)]

    (letfn [(write-to-memory [mem instruction]
              (if (mask-instruction? instruction)
                (assoc mem :mask instruction)
                (let [[address value] instruction]
                  (assoc mem address (->number
                                      (run-instruction value (:mask mem)))))))]

      (let [memory (reduce write-to-memory {} instructions)]

        (apply + (vals (dissoc memory :mask)))))))

(defn apply-mask [mask value]
  (let [binary-str (apply
                    str
                    (map #(if (or (= %1 \X) (= %1 \1))
                            %1
                            %2)
                         mask
                         (cl-format nil "~36,'0',B" value)))]
    binary-str))

(defn replace-str-at-pos-vals [string pos-vals]
  (let [sb (StringBuilder. string)]
    (doseq [[pos value] pos-vals]
      (.replace sb pos (inc pos) (str value)))
    (str sb)))

(defn ->bin-values [mask initial-address]
  (let [x-idxs (map first
                    (filter #(= (second %) \X)
                            (map vector (range) mask)))
        result (apply-mask mask initial-address)
        floating-combinations (apply cartesian-product
                                     (repeat (count x-idxs) [0 1]))
        ->memory-addresses (fn [floating-bits]
                             (replace-str-at-pos-vals
                              result
                              (map vector x-idxs floating-bits)))]

    (map ->memory-addresses floating-combinations)))

(defn solve-2 [raw-input]
  (let [instructions (parse-input raw-input)

        write-to-memory (fn [mem instruction]
                          (if (mask-instruction? instruction)

                            (assoc mem :mask instruction)

                            (let [[initial-address value] instruction

                                  write-to-memory2 (fn [mem2 bin-value]
                                                     (let [address (->number bin-value)]
                                                       (assoc mem2 address value)))]

                              (reduce write-to-memory2 mem (->bin-values
                                                            (:mask mem)
                                                            initial-address)))))

        memory (reduce write-to-memory {} instructions)]

    (apply + (vals (dissoc memory :mask)))))
