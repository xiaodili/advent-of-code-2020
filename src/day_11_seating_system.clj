(ns day-11-seating-system
  (:require
   [clojure.string :as str]))

(defn empty-seat? [cell]
  (= cell \L))

(defn occupied-seat? [cell]
  (= cell \#))

(defn floor? [cell]
  (= cell \.))

(def floor-cell \.)
(def occupied-seat \#)
(def empty-seat \L)

(defn ->char-list [string]
  (seq (char-array string)))

(defn update-seatings [seatings]
  (let [seatings-arr (to-array-2d seatings)]

    (letfn [(coord-occupied? [[i j]]
              (try (occupied-seat? (aget seatings-arr i j))
                   (catch ArrayIndexOutOfBoundsException _ false)))

            (update-cell [i j]
              (let [cell (aget seatings-arr i j)]
                (if (floor? cell)
                  floor-cell
                  (let [num-adjacent-occupied (count
                                               (filter
                                                coord-occupied?
                                                [[(dec i) j]
                                                 [(dec i) (dec j)]
                                                 [(dec i) (inc j)]
                                                 [i (dec j)]
                                                 [i (inc j)]
                                                 [(inc i) (dec j)]
                                                 [(inc i) j]
                                                 [(inc i) (inc j)]]))]
                    (if (empty-seat? cell)
                      (if (zero? num-adjacent-occupied)
                        occupied-seat
                        empty-seat)
                      (if (>= num-adjacent-occupied 4)
                        empty-seat
                        occupied-seat))))))

            (update-rows [i]
              (map #(update-cell i %)
                   (range (count (nth seatings i)))))]

      (map update-rows (range (count seatings))))))

(defn ->seatings [raw-input]
  (map ->char-list (str/split-lines raw-input)))

(defn quiesce-seatings [seatings & {:keys [update-fn]
                                    :or {update-fn update-seatings}}]
  (with-local-vars [current-seatings seatings,
                    next-seatings (update-seatings seatings)]
    (while (not (= @current-seatings @next-seatings))
      (var-set current-seatings @next-seatings)
      (var-set next-seatings (update-fn @next-seatings)))
    @current-seatings))

(defn num-occupied [seatings]
  (apply + (map
            #(count (filter occupied-seat? %))
            seatings)))

;; Seems kinda slow - took 112s user time
(defn solve-1 [raw-input]
  (let [stable-seatings (quiesce-seatings
                         (->seatings raw-input))]
    (num-occupied stable-seatings)))

(defn update-seatings-fov [seatings]
  (let [seatings-arr (to-array-2d seatings)
        height (count seatings)
        width (count (first seatings))]

    (letfn [(get-cell [i j]
              (try (aget seatings-arr i j)
                   (catch ArrayIndexOutOfBoundsException _ false)))

            (in-bounds? [i j]
              (and (>= i 0)
                   (>= j 0)
                   (< i height)
                   (< j width)))

            (ray-trace-occupied-seat? [pos velocity]
              ;; Returns whether an occupied seat is in sight
              (let [[_y _x] pos
                    [vy vx] velocity
                    start-x (+ _x vx)
                    start-y (+ _y vy)]
                (with-local-vars [x start-x,
                                  y start-y,
                                  cell (get-cell start-y start-x)]
                  (while (and
                          (in-bounds? @y @x)
                          (floor? @cell))
                    (do
                      (var-set x (+ @x vx))
                      (var-set y (+ @y vy))
                      (var-set cell (get-cell @y @x))))
                  (occupied-seat? @cell))))

            (update-cell [i j]
              (let [cell (aget seatings-arr i j)]

                (if (floor? cell)
                  floor-cell
                  (let [num-adjacent-occupied (count
                                               (filter
                                                #(ray-trace-occupied-seat? [i j] %)
                                                [[-1 0]
                                                 [-1 -1]
                                                 [-1 1]
                                                 [0 -1]
                                                 [0 1]
                                                 [1 -1]
                                                 [1 0]
                                                 [1 1]]))]
                    (if (empty-seat? cell)
                      (if (zero? num-adjacent-occupied)
                        occupied-seat
                        empty-seat)
                      (if (>= num-adjacent-occupied 5)
                        empty-seat
                        occupied-seat))))))

            (update-rows [i]
              (map #(update-cell i %)
                   (range width)))]

      (map update-rows (range height)))))

(defn solve-2 [raw-input]
  (let [stable-seatings (quiesce-seatings
                         (->seatings raw-input)
                         :update-fn update-seatings-fov)]
    (num-occupied stable-seatings)))
