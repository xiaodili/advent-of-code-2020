(ns day-17-conway-cubes
  (:require
   [clojure.string :as str]))

(defn ->p [space]
  (map #(mapv vec %) space))

(defn set-space! [space z y x value]
  (aset-char (nth space z) y x value))

(defn get-space [space z y x]
  (try
    (aget (nth space z) y x)
    ;; for Index and Array OutOfBoundsException
    (catch Exception _ \.)))

(defn empty-space [height depth width]
  (let [make-level (fn [_]
                     (make-array Character/TYPE depth width))
        space (map
               make-level
               (range height))]
    (doseq [x (range width)
            y (range depth)
            z (range height)]
      (set-space! space z y x \.))
    space))

(defn alive-cell? [cell]
  (= cell \#))

(defn evolve [configuration]
  (let [height (count configuration)
        depth (count (first configuration))
        width (count (first (first configuration)))
        new-height (+ height 2)
        new-depth (+ depth 2)
        new-width (+ width 2)
        configuration-array (map to-array-2d configuration)
        evolved-array (empty-space new-height new-depth new-width)
        get-cell (fn [z y x]
                   (get-space configuration-array z y x))
        update-cell (fn [new-z new-y new-x]
                      (let [old-z (dec new-z)
                            old-y (dec new-y)
                            old-x (dec new-x)
                            current-cell (get-cell old-z old-y old-x)
                            surrounding-cells [;; top slice
                                               (get-cell (dec old-z) (dec old-y) (dec old-x))
                                               (get-cell (dec old-z) (dec old-y) old-x)
                                               (get-cell (dec old-z) (dec old-y) (inc old-x))

                                               (get-cell (dec old-z) old-y (dec old-x))
                                               (get-cell (dec old-z) old-y old-x)
                                               (get-cell (dec old-z) old-y (inc old-x))

                                               (get-cell (dec old-z) (inc old-y) (dec old-x))
                                               (get-cell (dec old-z) (inc old-y) old-x)
                                               (get-cell (dec old-z) (inc old-y) (inc old-x))

                                               ;; middle slice
                                               (get-cell old-z (dec old-y) (dec old-x))
                                               (get-cell old-z (dec old-y) old-x)
                                               (get-cell old-z (dec old-y) (inc old-x))

                                               (get-cell old-z old-y (dec old-x))
                                               (get-cell old-z old-y (inc old-x))

                                               (get-cell old-z (inc old-y) (dec old-x))
                                               (get-cell old-z (inc old-y)  old-x)
                                               (get-cell old-z (inc old-y) (inc old-x))

                                               ;;bottom slice
                                               (get-cell (inc old-z) (dec old-y) (dec old-x))
                                               (get-cell (inc old-z) (dec old-y) old-x)
                                               (get-cell (inc old-z) (dec old-y) (inc old-x))

                                               (get-cell (inc old-z) old-y (dec old-x))
                                               (get-cell (inc old-z) old-y old-x)
                                               (get-cell (inc old-z) old-y (inc old-x))

                                               (get-cell (inc old-z) (inc old-y) (dec old-x))
                                               (get-cell (inc old-z) (inc old-y) old-x)
                                               (get-cell (inc old-z) (inc old-y) (inc old-x))]

                            num-alives (count (filter alive-cell? surrounding-cells))]

                        (if (alive-cell? current-cell)
                          (if (or (= num-alives 2) (= num-alives 3))
                            \#
                            \.)
                          (if (= num-alives 3)
                            \#
                            \.))))]

    (doseq [x (range new-width)
            y (range new-depth)
            z (range new-height)]
      (set-space! evolved-array z y x (update-cell z y x)))

    (->p evolved-array)))

(defn trim-top-face [space]
  (let [[top-face & remaining] space
        top-face-cells (flatten top-face)
        num-alive-cells (count (filter alive-cell? top-face-cells))]
    (if (zero? num-alive-cells)
      remaining
      space)))

(defn trim-bottom-face [space]
  (let [bottom-face (last space)
        bottom-face-cells (flatten bottom-face)
        num-alive-cells (count (filter alive-cell? bottom-face-cells))]
    (if (zero? num-alive-cells)
      (drop-last space)
      space)))

(defn trim-front-face [space]
  (let [front-face (map last space)
        front-face-cells (flatten front-face)
        num-alive-cells (count (filter alive-cell? front-face-cells))]
    (if (zero? num-alive-cells)
      (map drop-last space)
      space)))

(defn trim-back-face [space]
  (let [back-face (map first space)
        back-face-cells (flatten back-face)
        num-alive-cells (count (filter alive-cell? back-face-cells))]
    (if (zero? num-alive-cells)
      (map rest space)
      space)))

(defn trim-right-face [space]
  (let [right-face (map #(map last %) space)
        right-face-cells (flatten right-face)
        num-alive-cells (count (filter alive-cell? right-face-cells))]
    (if (zero? num-alive-cells)
      (map #(map drop-last %) space)
      space)))

(defn trim-left-face [space]
  (let [left-face (map #(map first %) space)
        left-face-cells (flatten left-face)
        num-alive-cells (count (filter alive-cell? left-face-cells))]
    (if (zero? num-alive-cells)
      (map #(map rest %) space)
      space)))

(defn trim-space [space]
  (-> space
      trim-back-face
      trim-front-face
      trim-top-face
      trim-bottom-face
      trim-right-face
      trim-left-face))

(defn next-configuration [configuration]
  (-> configuration
      evolve
      trim-space
      trim-space))

(defn ->char-list [string]
  (seq (char-array string)))

(defn ->initial-slice [raw-input]
  (map ->char-list (str/split-lines raw-input)))

(defn solve-1 [raw-input]
  (let [initial-slice (->initial-slice raw-input)
        initial-configuration [initial-slice]
        step (fn [state _]
               (next-configuration state))
        final-configuration (reduce
                             step
                             initial-configuration
                             (range 6))
        cells (flatten final-configuration)]

    (count (filter alive-cell? cells))))

(defn solve-2 [raw-input]
  2)
