(ns day-07-handy-haversacks
  (:require
   [clojure.string :as str]))

(defn str-drop-last [string num]
  (subs string 0 (- (count string) num)))

(defn parse-source [source-str]
  (str-drop-last source-str (count " bags")))

(defn parse-bag-frag [bag-str]
  (let [[num-str colour-str] (str/split bag-str #" " 2)
        num (read-string num-str)
        colour (first (str/split colour-str #" bag"))]
    [num colour]))

(defn no-bags? [bag-frag-strs]
  (= (first bag-frag-strs) "no other bags."))

(defn parse-destinations [destination-str]
  (let [bag-frags (str/split destination-str #", ")]
    (if (no-bags? bag-frags)
      []
      (map parse-bag-frag bag-frags))))

(defn ->rule [rule-str]
  (let [[source-frag dest-frag] (str/split rule-str #" contain ")]
    [(parse-source source-frag)
     (parse-destinations dest-frag)]))

(defn ->rules [raw-input]
  (let [rule-lines (str/split-lines raw-input)]
    (map ->rule rule-lines)))

(defn ->rules-lookup [rules]
  (with-local-vars [rules-lookup {}]
    (doseq [rule rules]
      (let [[source destinations] rule]
        (doseq [[_ destination] destinations]
          (var-set rules-lookup
                   (assoc @rules-lookup destination
                          (cons source (get @rules-lookup destination [])))))))
    @rules-lookup))

(defn traversable-to [rules-lookup start]
  (with-local-vars [visited #{}, to-visit [start]]
    (while (not-empty @to-visit)
      (let [visiting (first @to-visit)
            destinations (get rules-lookup visiting [])
            new-destinations (filter #(not (contains? @visited %)) destinations)]
        (var-set visited (conj @visited visiting))
        (var-set to-visit (concat new-destinations (rest @to-visit)))))
    (dec ; Removing the one we started from
     (count @visited))))

(defn solve-1 [raw-input]
  (let [rules (->rules raw-input)
        rules-lookup (->rules-lookup rules)]
    (traversable-to rules-lookup "shiny gold")))

(defn num-bags-in [rules start]
  (let [rules-lookup (into {} rules)]
    (letfn [(num-children [weight bag-name]
              (let [children (get rules-lookup bag-name)]
                (+ weight
                   (* weight
                      (apply + (map #(apply num-children %) children))))))]
      (dec ; Removing the one we started from
       (num-children 1 start)))))

(defn solve-2 [raw-input]
  (let [rules (->rules raw-input)]
    (num-bags-in rules "shiny gold")))
