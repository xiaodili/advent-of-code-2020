(ns day-19-monster-messages
  (:require
   [clojure.string :as str]))

(defn ->rule-body [rule-body-str]

  (if (or (= rule-body-str "\"a\"") (= rule-body-str "\"b\""))

    (second rule-body-str)

    (let [subrule-strs (str/split rule-body-str #" \| ")
          parse-subrule (fn [subrule-str]
                          (map read-string
                               (str/split subrule-str #" ")))
          subrules (map parse-subrule subrule-strs)]

      subrules)))

(defn ->rules-db [rules-frag]
  (let [rule-lines (str/split-lines rules-frag)
        process-rule-line (fn [rules rule-line]
                            (let [[rule-name rule-body-str] (str/split rule-line #": ")]
                              (assoc rules (read-string rule-name) (->rule-body rule-body-str))))]
    
    (reduce process-rule-line {} rule-lines)))


;; BFS on traversing rule matching. Should probably do DFS for memory
(defn match [rules-db message & {:keys [rule-start]
                                 :or {rule-start 0}}]

  (with-local-vars [found false, to-visit-paths [[message [rule-start]]]]
    
    (while (and (not @found) (seq @to-visit-paths))

      (let [[visit-path & remaining-paths] @to-visit-paths
            [msg rules] visit-path]

        (var-set to-visit-paths remaining-paths)

        (when (and (empty? msg) (empty? rules))
          (var-set found true))

        (when (and (seq rules) (seq msg))
          (let [[rule & remaining-rules] rules
                resolved-rule (get rules-db rule)]

            (if (char? resolved-rule)
              (let [[c & remaining-msg] msg]
                (when (= c resolved-rule)
                  (var-set to-visit-paths (conj @to-visit-paths [remaining-msg remaining-rules]))))

              (let [new-ruless (map #(concat % remaining-rules) resolved-rule)]

                (doseq [new-rules new-ruless]
                  (var-set to-visit-paths (conj @to-visit-paths [msg new-rules])))))))))
    @found))

(defn solve-1 [raw-input]
  (let [[rules-frag messages-frags] (str/split raw-input #"\n\n")
        rules-db (->rules-db rules-frag)
        messages (str/split-lines messages-frags)
        matches (map #(match rules-db %) messages)]

    (count (filter true? matches))))

;; Didn't to need to change implementation to deal with loops; think it's because I'm using BFS. Phew good thing I was too lazy to prematurely optimise!
(defn solve-2 [raw-input]
  (let [[rules-frag messages-frags] (str/split raw-input #"\n\n")
        rules-db (->rules-db rules-frag)
        new-rules-db (assoc rules-db
                            8 [[42] [42 8]]
                            11 [[42 31] [42 11 31]])
        messages (str/split-lines messages-frags)
        matches (map #(match new-rules-db %) messages)]

    (count (filter true? matches))))
