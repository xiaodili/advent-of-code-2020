(ns day-01-report-repair
  (:require
   [clojure.string :refer [split-lines]]
   [clojure.math.combinatorics :refer [combinations]]))

(defn parse-input [raw-input]
  (map read-string
       (split-lines raw-input)))

(defn find-sum-to [target entries num-entries-to-pick]
  (some #(when (= target (apply + %)) %) (combinations entries num-entries-to-pick)))

(defn solve-1 [raw-input]
  (let [expenses (parse-input raw-input)
        pair (find-sum-to 2020 expenses 2)]
    (apply * pair)))

(defn solve-2 [raw-input]
  (let [expenses (parse-input raw-input)
        pair (find-sum-to 2020 expenses 3)]
    (apply * pair)))
