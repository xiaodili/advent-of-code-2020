(ns day-16-ticket-translation
  (:require
   [clojure.set :refer [intersection]]
   [clojure.string :as str]))

(defn ->field [field-str]
  (let [[_ field-name l1 u1 l2 u2] (re-matches #"([a-z ]+): (\d+)-(\d+) or (\d+)-(\d+)" field-str)]
    [field-name
     [(Integer/parseInt l1) (Integer/parseInt u1)]
     [(Integer/parseInt l2) (Integer/parseInt u2)]]))

(defn ->fields [raw-fields]
  (let [fields-str (str/split-lines raw-fields)]
    (map ->field fields-str)))

(defn ->tickets [raw-tickets]
  (map read-string
       (str/split 
        raw-tickets #",")))

(defn ->your-tickets [raw-your-tickets]
  (->tickets
   (last
    (str/split-lines raw-your-tickets))))

(defn ->nearby-tickets [raw-nearby-tickets]
  (let [[_ & ticket-lines] (str/split-lines raw-nearby-tickets)]
    (map ->your-tickets ticket-lines)))

(defn parse-input [raw-input]
  (let [[raw-fields raw-your-ticket raw-nearby-tickets] (str/split raw-input #"\n\n")]
    [(->fields raw-fields)
     (->your-tickets raw-your-ticket)
     (->nearby-tickets raw-nearby-tickets)]))

(defn in-range? [ranges ticket]
  (some #(let [[lower upper] %]
           (and (>= ticket lower) (<= ticket upper)))
        ranges))

(defn ->invalid-tickets [ranges tickets]
  (filter (comp not (partial in-range? ranges)) tickets))

(defn solve-1 [raw-input]
  (let [[fields _ nearby-tickets] (parse-input raw-input)
        ranges (mapcat rest fields)
        tickets (flatten nearby-tickets)
        invalid-tickets (->invalid-tickets ranges tickets)]
    (apply + invalid-tickets)))

(defn all-valid? [ranges values]
  (every? #(in-range? ranges %) values))

;; Assumes there's exactly one way of assigning columns to fields (see diagnostics output)
(defn ->column-field-idxs [rangess tickets]

  (let [columns (apply map list tickets)
        idx-columns (into {} (map vector (range) columns))
        idx-rangess (into {} (map vector (range) rangess))]

    (with-local-vars [remaining-columns idx-columns,
                      remaining-rangess idx-rangess,
                      column-field-idxs []]

      (let [one-valid-ranges? (fn [[column-idx column-vals]]

                                (let [matching-ranges (filter #(all-valid? (second %) column-vals)
                                                              (into [] @remaining-rangess))]

                                  (if (= 1 (count matching-ranges))
                                    [column-idx (first (first matching-ranges))]
                                    nil)))]

        (while (not-empty @remaining-columns)

          (let [[column-idx range-idx] (some one-valid-ranges? (into [] @remaining-columns))]

            (var-set remaining-columns (dissoc @remaining-columns column-idx))
            (var-set remaining-rangess (dissoc @remaining-rangess range-idx))
            (var-set column-field-idxs (conj @column-field-idxs [column-idx range-idx]))))

        @column-field-idxs))))

;; (map #(count (filter identity %)) diagnostics):  (11 20 2 17 16 13 8 5 15 19 9 1 18 4 12 14 6 10 3 7)
(defn ->diagnostics [rangess tickets]
  (let [columns (apply map list tickets)
        rangess-valid? (fn [column]
                         (map #(all-valid? % column) rangess))]
    (map rangess-valid? columns)))

(defn solve-2 [raw-input]

  (let [[fields my-tickets nearby-tickets] (parse-input raw-input)

        all-ranges (mapcat rest fields)
        all-tickets (flatten nearby-tickets)
        invalid-values (into #{} (->invalid-tickets all-ranges all-tickets))

        rangess (map rest fields)

        valid-tickets (filter #(empty? (intersection invalid-values (into #{} %))) nearby-tickets)

        all-tickets (conj valid-tickets my-tickets)
        column-field-idxs (->column-field-idxs rangess all-tickets)

        departure-field? (fn [[_ field]]
                             (str/starts-with? (first field) "departure"))

        departure-field-idxs (map first (filter departure-field? (map vector (range) fields)))

        ->my-ticket-value (fn [field-idx]

                            (let [column-idx (some #(when (= field-idx (second %))
                                                          (first %)) column-field-idxs)]
                              (nth my-tickets column-idx)))

        my-ticket-values (map ->my-ticket-value departure-field-idxs)]

    (apply * my-ticket-values)))
