(ns day-02-password-philosophy
  (:require
   [clojure.string :as str]))

(defn parse-policy-password [policy-password-str]
  (let [[range character-str password] (str/split policy-password-str #" ")
        [least most] (str/split range #"-")]
    [{:character (first character-str)
      :at-least (read-string least)
      :at-most (read-string most)} password]))

(defn parse-input [raw-input]
  (map parse-policy-password
       (str/split-lines raw-input)))

(defn password-valid? [policy password]
  (let [{:keys [character at-least at-most]} policy
        character-count ((frequencies password) character 0)]
    (and (>= character-count at-least) (<= character-count at-most))))

;; Eh let's just use the same policy map object
(defn password-official-valid? [policy password]
  (let [{:keys [character at-least at-most]} policy
        first-pos (nth password (dec at-least))
        second-pos (nth password (dec at-most))]

    ;; xor
    (and (or (= first-pos character)
             (= second-pos character))
         (not (and (= first-pos character)
                   (= second-pos character))))))

(defn solve-1 [raw-input]
  (let [policy-passwords (parse-input raw-input)]
    (count (filter #(let [[policy password] %]
                      (password-valid? policy password)) policy-passwords))))

(defn solve-2 [raw-input]
  (let [policy-passwords (parse-input raw-input)]
    (count (filter #(let [[policy password] %]
                      (password-official-valid? policy password)) policy-passwords))))
