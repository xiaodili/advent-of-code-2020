(ns day-04-passport-processing
  (:require
   [clojure.string :as str]
   [clojure.set :refer [difference]]))

;; https://stackoverflow.com/questions/34249647/what-are-the-benefits-of-using-keywords-as-keys-in-maps-in-clojure
(defn parse-key-value-str [key-value-str]
  (let [[k v] (str/split key-value-str #":")]
    [(keyword k) v]))

(defn parse-passport-str [passport-str]
  (let [key-value-strs (str/split passport-str #" ")
        key-values (map parse-key-value-str key-value-strs)]
    (into {} key-values)))

(defn contains-passport-details? [line]
  (not (str/blank? line)))

(defn parse-input [raw-input]
  (with-local-vars [passports nil, current-passport {}]
    (doseq [line (str/split-lines raw-input)]
      (if (contains-passport-details? line)
        (var-set current-passport (into @current-passport (parse-passport-str line)))
        (do
          (var-set passports (cons @current-passport @passports))
          (var-set current-passport {}))))
    (var-set passports (cons @current-passport @passports))
    (reverse @passports)))

;; Omitting CID
(def required-fields [:byr :iyr :eyr :hgt :hcl :ecl :pid])

(defn valid-passport? [passport]
  (empty? (difference (set required-fields) (set (keys passport)))))

;; https://stackoverflow.com/questions/5621279/in-clojure-how-can-i-convert-a-string-to-a-number
(defn parse-int [int-maybe-str]
  (try
    (let [num (Integer/parseInt int-maybe-str)]
      [num true])
    (catch NumberFormatException _ [nil false])))

(defn in-range? [num low high]
  (and (>= num low) (<= num high)))

(defn valid-byr? [byr-str]
  (let [[num is-int] (parse-int byr-str)]
    (and is-int (in-range? num 1920 2002))))

(defn valid-iyr? [iyr-str]
  (let [[num is-int] (parse-int iyr-str)]
    (and is-int (in-range? num 2010 2020))))

(defn valid-eyr? [eyr-str]
  (let [[num is-int] (parse-int eyr-str)]
    (and is-int (in-range? num 2020 2030))))

(defn valid-cm-hgt? [hgt-str]
  (let [[num is-int] (parse-int hgt-str)]
    (and is-int (in-range? num 150 193))))

(defn valid-inches-hgt? [hgt-str]
  (let [[num is-int] (parse-int hgt-str)]
    (and is-int (in-range? num 59 76))))

(defn str-drop-last [string num]
  (subs string 0 (- (count string) num)))

(defn valid-hgt? [hgt-str]
  (cond
    (str/ends-with? hgt-str "in") (valid-inches-hgt? (str-drop-last hgt-str 2))
    (str/ends-with? hgt-str "cm") (valid-cm-hgt? (str-drop-last hgt-str 2))
    :else false))

(defn valid-hcl? [hcl-str]
  (and
   (= (count hcl-str) 7)
   (= (first hcl-str) \#)
   (string? (re-matches #"[0-9a-f]{6}" (subs hcl-str 1)))))

(def valid-eyrs #{"amb" "blu" "brn" "gry" "grn" "hzl" "oth"})
(defn valid-ecl? [ecl-str]
  (contains? valid-eyrs ecl-str))

(defn valid-pid? [pid-str]
  (and
   (= (count pid-str) 9)
   (second (parse-int pid-str))))

(defn strict-valid-passport? [passport]
  (and (valid-passport? passport)
       (valid-byr? (:byr passport))
       (valid-iyr? (:iyr passport))
       (valid-eyr? (:eyr passport))
       (valid-hgt? (:hgt passport))
       (valid-hcl? (:hcl passport))
       (valid-ecl? (:ecl passport))
       (valid-pid? (:pid passport))))

(defn solve-1 [raw-input]
  (let [passports (parse-input raw-input)]
    (count (filter valid-passport? passports))))

(defn solve-2 [raw-input]
  (let [passports (parse-input raw-input)]
    (count (filter strict-valid-passport? passports))))
