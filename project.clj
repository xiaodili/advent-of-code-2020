(defproject aoc-2020 "1.0.0"

  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [org.flatland/ordered "1.5.9"]]

  :profiles {:dev {:dependencies [[org.clojure/tools.trace "0.7.10"]]}}

  :main runner)
