# Advent of Code 2020

Solutions in Clojure (and TypeScript)

## Tests

    lein test
    
Or for a specific test (note the `>` is escaped):

    lein test :only day-07-handy-haversacks-test/-\>rules-lookup-test

## Running

To run e.g. Day 1 part 2:

    lein run 1-2 data/01_input.txt

## Development

Currently in Emacs with CIDER and clj-kondo

* `<C-c> <C-k>` for `cider-load-buffer` on test file(s) initially
* `cider-refresh` for subsequent implementation/test file changes
* `<C-c> <C-t> l` for `cider-run-loaded-tests`
* `#_` for commenting out forms/tests (need to `cider-refresh` after)

## TypeScript Solutions

Located in `ts-solutions`.  Install with `npm i`.

Run with e.g.:

```sh
npx ts-node src/day21.ts
```

