def parse(r):
    f,a=r[:-1].split(" (contains")
    a=a.replace(" ","").split(",")
    return set(f.split()),set(a)

def answers(raw):
    fa=list(zip(*(parse(r) for r in raw.split("\n"))))
    print('fa', fa)
    print('list(zip(*fa))', list(zip(*fa)))
    
    
    all_ings,all_alls=(set.union(*x) for x in fa)
    print('all_ings', all_ings)
    print('all_alls', all_alls)
    uniques={key:set.intersection(*(f for f,a in zip(*fa) if key in a))\
             for key in all_alls}
    print('uniques', uniques)
    

    free_ai=[]
    while uniques:
        kr,rv=next((k,v) for k,v in uniques.items() if len(v)==1)
        free_ai.append((kr,list(uniques.pop(kr))[0]))
        for v in uniques.values(): v-=rv

    _,free_ings=zip(*sorted(free_ai))
    dang_ings=all_ings-set(free_ings)

    yield sum(sum(1 for ing in food if ing in dang_ings) for food in fa[0])
    yield ",".join(free_ings)

sample_input = """mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)"""

print(list(answers(sample_input)))