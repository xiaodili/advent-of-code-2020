import * as fs from 'fs'

const sampleInput = `mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`

// Create a type called FoodLine
// It should have two properties: ingredients and allergens
// Both should be arrays of strings
type FoodLine = {
    ingredients: string[],
    allergens: string[]
}

function getNonAllergenicFoodCount(foodLines: FoodLine[]): number {
    // Create set of ingredients
    const ingredients = new Set<string>();
    const allergens = new Set<string>();

    for (const foodLine of foodLines) {
        for (const ingredient of foodLine.ingredients) {
            ingredients.add(ingredient)
        }
        for (const allergen of foodLine.allergens) {
            allergens.add(allergen);
        }
    }

    const allergenIngredients = new Map<string, Set<string>>()

    for (const allergen of allergens) {
        for (const foodLine of foodLines) {
            if (foodLine.allergens.includes(allergen)) {
                if (!allergenIngredients.has(allergen)) {
                    allergenIngredients.set(allergen, new Set<string>(foodLine.ingredients))
                } else {
                    const intersect = new Set<string>()
                    for (const food of foodLine.ingredients) {
                        if (allergenIngredients.get(allergen).has(food)) {
                           intersect.add(food) 
                        }
                    }
                    allergenIngredients.set(allergen, intersect)
                }
            }
        }
    }
    const allergenicIngredients = new Set<string>()
    const allergenicIngredientsByAllergy = new Set<[string, string]>()

    while (allergenIngredients.size !== 0) {
        for (const [allergen, ingredients] of allergenIngredients) {
            if (ingredients.size === 1) {
                const ingredient = Array.from(ingredients)[0]
                for (const [allergen2, ingredients2] of allergenIngredients) {
                    if (allergen !== allergen2) {
                        ingredients2.delete(ingredient)
                    }
                }
                allergenicIngredientsByAllergy.add([allergen, ingredient])
                allergenicIngredients.add(ingredient)
                allergenIngredients.delete(allergen)
            }
        }
    }

    // sort allergicIngredients alphabetically
    const sortedAllergenicIngredients = Array.from(allergenicIngredientsByAllergy).sort((a, b) => {
        if (a[0] < b[0]) {
            return -1
        } else if (a[0] > b[0]) {
            return 1
        } else {
            return 0
        }
    }).map(x => x[1])
    console.log('sortedAllergenicIngredients: ', sortedAllergenicIngredients.join(","));

    let numSafeIngredients = 0
    for (const foodLine of foodLines) {
        for (const ingredient of foodLine.ingredients) {
            if (!allergenicIngredients.has(ingredient)) {
                numSafeIngredients += 1
            }
        }
    }



    return numSafeIngredients
}

function parseInput(input: string): FoodLine[] {
    // Split the input into lines
    const lines = input.split('\n');
    // For each line, split it into ingredients and allergens
    // Then return an object with those two arrays
    return lines.map(line => {
        const [ingredients, allergens] = line.split(' (contains ');
        
        return {
            ingredients: ingredients.trim().split(' '),
            allergens: allergens.trim().slice(0, -1).split(', ')
        }
    })
}

const sampleNonAllergenicFood = getNonAllergenicFoodCount(parseInput(sampleInput))
console.log('sampleNonAllergenicFood', sampleNonAllergenicFood);

const input = fs.readFileSync('../data/21_input.txt').toString().trim();
const foodLines = parseInput(input)
const nonAllergenicFoodCount = getNonAllergenicFoodCount(foodLines)
console.log('nonAllergenicFoodCount', nonAllergenicFoodCount);