import fs from 'fs'
type Deck = Array<number>;

const samplePlayer1Deck: Deck = [9, 2, 6, 3, 1]
const samplePlayer2Deck: Deck = [5, 8, 4, 7, 10]

class Player {
    deck: Deck
    constructor(deck: Deck) {
        this.deck = deck
    }
    playCard(): number {
        return this.deck.shift()
    }
    addCardsToBottom(cards: Array<number>) {
        this.deck.push(...cards)
    }
    getScore(): number {
        return this.deck.reduce((score, card, index) => score + (card * (this.deck.length - index)), 0)
    }
}

class Game {
    player1: Player
    player2: Player
    constructor(player1: Player, player2: Player) {
        this.player1 = player1
        this.player2 = player2
    }
    playRound(): boolean {
        const player1Card = this.player1.playCard()
        const player2Card = this.player2.playCard()
        if (player1Card > player2Card) {
            this.player1.addCardsToBottom([player1Card, player2Card])
        } else {
            this.player2.addCardsToBottom([player2Card, player1Card])
        }
        return this.player1.deck.length === 0 || this.player2.deck.length === 0
    }
    playGame(): Player {
        while (!this.playRound()) {}
        return this.player1.deck.length === 0 ? this.player2 : this.player1
    }
}

const game = new Game(new Player(samplePlayer1Deck), new Player(samplePlayer2Deck))
const winningPlayer = game.playGame()
const score = winningPlayer.getScore()
console.log('score: ', score);

function parseInput(inputPath: string): [Deck, Deck] {
    const input = fs.readFileSync(inputPath, 'utf-8').replace(/\r/g, "")
    
    const [player1Input, player2Input] = input.split("\n\n")
    
    const player1Deck = player1Input.split("\n").slice(1).map(x => parseInt(x))
    const player2Deck = player2Input.split("\n").slice(1).map(x => parseInt(x))
    return [player1Deck, player2Deck]
}

const [d1, d2] = parseInput("../data/22_input.txt")
const g = new Game(new Player(d1), new Player(d2))
const p = g.playGame()
const s = p.getScore()
console.log('s: ', s);
