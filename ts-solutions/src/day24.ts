import fs from 'fs'
type TileColour = 'black' | 'white'

type TileDirection = 'e' | 'w' | 'ne' | 'nw' | 'se' | 'sw' 

const oppositeDirection: {[k in TileDirection]: TileDirection} = {
    'e': 'w',
    'w': 'e',
    'ne': 'sw',
    'sw': 'ne',
    'se': 'nw',
    'nw': 'se',
}

// This doesn't work; need to take into consideration
class Tile {
    colour: TileColour = 'white'
    blackCounter: BlackCounter
    neighbours: {[k in TileDirection]?: Tile} = {
    }
    constructor(blackCounter: BlackCounter) {
        this.blackCounter = blackCounter
    }
    traverse(d: TileDirection): Tile {
        if (!this.neighbours[d]) {
            this.neighbours[d] = new Tile(this.blackCounter)
            this.neighbours[d].neighbours[oppositeDirection[d]] = this
        }
        return this.neighbours[d]
    }
    flip() {
        if (this.colour === 'white') {
            this.colour = 'black'
            this.blackCounter.inc()
        } else if (this.colour === 'black') {
            this.colour = 'white'
            this.blackCounter.dec()
        }
    }
}

const sampleInput = `sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew`


function parseDirections(input: string): TileDirection[][] {
    const tileDirectionStrings = input.split('\n')
    return tileDirectionStrings.map(s => {
        const directions: TileDirection[] = []
        let i = 0
        while (i < s.length) {
            if (s[i] === 'e' || s[i] === 'w') {
                directions.push(s[i] as TileDirection)
                i += 1
            } else {
                directions.push(s.slice(i, i+2) as TileDirection)
                i += 2
            }
        }
        return directions
    })
}

const sampleDirections = parseDirections(sampleInput)

class BlackCounter {
   count: number = 0 
   inc() {
    this.count += 1
   }
   dec() {
    this.count -= 1
   }
}

function countBlackTiles(directionsList: TileDirection[][]): number {
    const blackCounter = new BlackCounter()
    const centralTile = new Tile(blackCounter)
    for (const directions of directionsList) {
        let currentTile = centralTile
        for (const direction of directions) {
           currentTile = currentTile.traverse(direction) 
        }
        currentTile.flip()
    }
    return blackCounter.count
}

// console.log('countBlackTiles(sampleDirections): ', countBlackTiles(sampleDirections));
// console.log('countBlackTiles(sampleDirections): ', countBlackTiles([['nw', 'w', 'sw', 'e', 'e'], [], []]));

class HexGrid {
    // odd-r horizontal layout
    coordinates: {
        [column: number]: {
            [row: number]: TileColour
        }
    } = {}

    traverseAndFlip(directions: TileDirection[]) {
        let currentColumn = 0
        let currentRow = 0
        for (const d of directions) {
            if (d === 'e') {
               currentColumn += 1 
            } else if (d === 'w') {
               currentColumn -= 1 
            } else if (d === 'nw') {
                if (Math.abs(currentRow % 2) === 0) {
                    currentColumn -= 1
                }
               currentRow -= 1 
            } else if (d === 'sw') {
                if (Math.abs(currentRow % 2) === 0) {
                    currentColumn -= 1
                }
               currentRow += 1 
            } else if (d === 'ne') {
                if (Math.abs(currentRow % 2) === 1) {
                    currentColumn += 1
                }
                currentRow -= 1
            } else if (d === 'se') {
                if (Math.abs(currentRow % 2) === 1) {
                    currentColumn += 1
                }
                currentRow += 1
            }

            if (!this.coordinates[currentColumn]) {
                this.coordinates[currentColumn] = {}
            }
            if (!this.coordinates[currentColumn][currentRow]) {
                this.coordinates[currentColumn][currentRow] = 'white'
            }
        }

        // Flipping
        if (this.coordinates[currentColumn][currentRow] === 'white') {
            this.coordinates[currentColumn][currentRow] = 'black'
        } else {
            this.coordinates[currentColumn][currentRow] = 'white'
        }
    }

    getBlackCount(): number {
        let blackCount = 0
        for (const column in this.coordinates) {
            for (const row in this.coordinates[column]) {
                if (this.coordinates[column][row] === 'black') {
                    blackCount += 1
                }
            }
        }
        return blackCount
    }
}

// Iterate over characters a-z
// function* charGenerator() {
//     for (let i = 97; i <= 122; i++) {
    

function countBlackTiles2(directionsList: TileDirection[][]): number {
    const hexGrid = new HexGrid()
    for (const directions of directionsList) {
        hexGrid.traverseAndFlip(directions)
    }
    return hexGrid.getBlackCount()
}

console.log('countBlackTiles2(sampleDirections): ', countBlackTiles2(sampleDirections));
console.log('countBlackTiles2(no-op): ', countBlackTiles2([['nw', 'w', 'sw', 'e', 'e'], []]));

console.log('countBlackTiles2(real): ', countBlackTiles2(parseDirections(fs.readFileSync("../data/24_input.txt", 'utf-8').replace(/\r/g, ""))))